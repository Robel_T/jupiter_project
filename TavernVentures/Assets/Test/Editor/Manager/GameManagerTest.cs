﻿using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript
{
    [Test]
    public void TheGameManagerShouldHaveTheCorrectState()
    {
        GameManager gm = new GameManager();
        State s = new State();
        gm.SetState(s);
        Assert.AreEqual(s, gm.currentState);
    }

    [Test]
    public void TheGameManagerShouldBeCapableToConvertAJSONStringToCards()
    {
        GameManager gm = new GameManager();
        List<string> wanted = new List<string>();
        wanted.Add("D2");
        wanted.Add("B13");
        wanted.Add("N8");
        wanted.Add("Y12");
        gm.MakeHand("{\"hand\":[{\"card\":\"D2\"}," +
                "{\"card\":\"B13\"},{\"card\":\"N8\"}," +
                "{\"card\":\"Y12\"}]}");
        for(int i = 0; i < wanted.Count; ++i)
        {
            Assert.AreEqual(wanted[i], gm.myHand.hand[i].card);
        }
    }

    [Test]
    public void TheGameManagerShouldBeCapableToConvertAJSONStringToBets()
    {
        GameManager gm = new GameManager();
        List<int> wantedId = new List<int>();
        wantedId.Add(0);
        wantedId.Add(1);
        wantedId.Add(4);
        wantedId.Add(6);
        List<int> wantedBet = new List<int>();
        wantedBet.Add(8);
        wantedBet.Add(0);
        wantedBet.Add(3);
        wantedBet.Add(0);
        gm.ReceivedPlayersBets("{\"bets\":[{\"bet\":8,\"id\":0},{\"bet\":0,\"id\":1},{\"bet\":3,\"id\":4}" +
            ",{\"bet\":0,\"id\":6}]}");
        for (int i = 0; i < wantedId.Count; ++i)
        {
            Assert.AreEqual(wantedId[i], gm.bets.bets[i].id);
            Assert.AreEqual(wantedBet[i], gm.bets.bets[i].bet);
        }
    }

    [Test]
    public void TheGameManagerShouldHaveTheRightNewPlay()
    {
        GameManager gm = new GameManager();
        gm.turnColor = CardType.Colorless;
        gm.ReceivedPlay("{\"newPlay\":{\"card\":\"D2\",\"player\":4}}", "{\"leader\":{\"card\":\"N13\",\"player\":2}}", "RED");
        Assert.AreEqual(CardType.Red, gm.turnColor);
        Assert.AreEqual("D2", gm.play.newPlay.card);
        Assert.AreEqual(4, gm.play.newPlay.player);
        Assert.AreEqual("N13", gm.newLeader.leader.card);
        Assert.AreEqual(2, gm.newLeader.leader.player);
    }

    [Test]
    public void TheGameManagerShouldHaveTheRightTurnWinner()
    {
        GameManager gm = new GameManager();
        gm.ReceivedTurnwinner(1);
        Assert.AreEqual(1, gm.turnwinner);
    }

    [Test]
    public void TheGameManagerShouldExcludeTheRightPlayer()
    {
        GameManager gm = new GameManager();
        gm.ReceivedExcluded(4);
        Assert.AreEqual(4, gm.newExcluded);
    }

    [Test]
    public void TheGameManagerShouldHaveTheRightPoints()
    {
        GameManager gm = new GameManager();
        List<int> wantedId = new List<int>();
        wantedId.Add(0);
        wantedId.Add(1);
        wantedId.Add(3);
        wantedId.Add(4);
        List<int> wantedPoints = new List<int>();
        wantedPoints.Add(120);
        wantedPoints.Add(-70);
        wantedPoints.Add(0);
        wantedPoints.Add(290);
        gm.ReceivedPoints("{\"playerPoints\":[{\"id\":0,\"points\":120}," +
                "{\"id\":1,\"points\":-70},{\"id\":3,\"points\":0},{\"id\":4,\"points\":290}]}");
        for (int i = 0; i < wantedId.Count; ++i)
        {
            Assert.AreEqual(wantedId[i], gm.point.playerPoints[i].id);
            Assert.AreEqual(wantedPoints[i], gm.point.playerPoints[i].points);
        }
    }
}
