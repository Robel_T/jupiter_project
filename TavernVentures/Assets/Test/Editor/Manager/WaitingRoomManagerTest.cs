﻿using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitingRoomManagerTest
{
    [Test]
    public void TheWaitingRoomManagerShouldHaveTheRightRoomName()
    {
        WaitingRoomManager wrm = new WaitingRoomManager();
        wrm.ManageRoom("Right", "{\"players\":[]}");
        Assert.AreEqual("Right", wrm.id);
    }

    [Test]
    public void TheWaitingRoomManagerShouldHaveTheRightPlayers()
    {
        WaitingRoomManager wrm = new WaitingRoomManager();
        Settings.GetPlayersManager().createPlayer("", null, 0);
        Settings.GetPlayersManager().createPlayer("", null, 1);
        Settings.GetPlayersManager().createPlayer("", null, 2);
        Settings.GetPlayersManager().createPlayer("", null, 3);
        List<string> name = new List<string>();
        name.Add("Julien");
        name.Add("Loris");
        name.Add("Robel");
        name.Add("Yoann");
        List<int> id = new List<int>();
        id.Add(0);
        id.Add(1);
        id.Add(2);
        id.Add(3);
        wrm.ManageRoom("Right", "{\"players\":[{\"name\":\"Julien\",\"id\":0}," +
            "{\"name\":\"Loris\",\"id\":1},{\"name\":\"Robel\",\"id\":2}," +
            "{\"name\":\"Yoann\",\"id\":3}]}");
        for(int i = 0; i < id.Count; ++i)
        {
            Assert.AreEqual(name[i], wrm.pl.players[i].name);
            Assert.AreEqual(id[i], wrm.pl.players[i].id);
        }
    }
}
