﻿using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersManagerTest
{
    [Test]
    public void ThePlayersManagerShouldBeAbleToCreateAPlayer()
    {
        PlayersManager pm = new PlayersManager();
        pm.createPlayer("Jon Doe", null, 42);
        Assert.AreEqual("Jon Doe", pm.allPlayers[0].name);
        Assert.AreEqual(null, pm.allPlayers[0].icon);
        Assert.AreEqual(42, pm.allPlayers[0].id);
    }

    [Test]
    public void ThePlayersManagerShouldBeAbleToCreateALocalPlayer()
    {
        PlayersManager pm = new PlayersManager();
        pm.createLocalPlayer("Jon Doe");
        Assert.AreEqual("Jon Doe", pm.GetCurrentPlayer().name);
    }

    [Test]
    public void ThePlayersManagerShouldBeAbleToTellIfAPlayerExistsOrNot()
    {
        PlayersManager pm = new PlayersManager();
        pm.createLocalPlayer("Jon Doe");
        pm.createPlayer("Julien Rod", null, 418);
        Assert.AreEqual(true, pm.doesPlayerExist(6));
        Assert.AreEqual(true, pm.doesPlayerExist(418));
        Assert.AreEqual(false, pm.doesPlayerExist(404));
    }

    [Test]
    public void ThePlayersManagerShouldReturnThePlayerById()
    {
        PlayersManager pm = new PlayersManager();
        pm.createLocalPlayer("Jon Doe");
        pm.createPlayer("Julien Rod", null, 418);
        pm.createPlayer("Yoann Rohrbasser", null, 404);
        pm.createPlayer("Robet Teklehaimanot", null, 302);
        pm.createPlayer("Loris Crull", null, 500);
        Assert.AreEqual("Julien Rod", pm.GetPlayerById(418).name);
        Assert.AreEqual(null, pm.GetPlayerById(8));
        Assert.AreEqual("Yoann Rohrbasser", pm.GetPlayerById(404).name);
        Assert.AreEqual("Loris Crull", pm.GetPlayerById(500).name);
        Assert.AreEqual("Jon Doe", pm.GetPlayerById(6).name);
        Assert.AreEqual("Robet Teklehaimanot", pm.GetPlayerById(302).name);
    }
}
