﻿using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoinAPartyTest
{
    [Test]
    public void InitReceivedShouldBeTrueAfterTheReceptionOfAnInit()
    {
        JoinAParty jap = new JoinAParty();
        jap.InitReceived();
        Assert.AreEqual(true, jap.initReceived);
    }

    [Test]
    public void WrongCodeShouldBeTrueAfterTheReceptionOfAnInit()
    {
        JoinAParty jap = new JoinAParty();
        jap.WrongCode();
        Assert.AreEqual(true, jap.isWrong);
    }
}
