﻿using NSubstitute;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitAPartyTest
{

    [Test]
    public void InitReceivedShouldBeTrueAfterTheReceptionOfAnInit()
    {
        InitAParty iap = new InitAParty();
        iap.InitReceived();
        Assert.AreEqual(true, iap.initReceived);
    }
}
