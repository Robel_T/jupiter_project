﻿using UnityEngine;
using System.Collections;

public abstract class GameElementsLogic : ScriptableObject
{
    public abstract void OnClick(CardInstance inst);
}
