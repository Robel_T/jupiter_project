﻿using UnityEngine;
using System.Collections;

public class CardInstance : MonoBehaviour, IClickable
{
    public CardDisplay cardDisplay;
    public GameElementsLogic currentLogic;

    public void Start()
    {
        cardDisplay = GetComponent<CardDisplay>();
    }

    public void OnClick()
    {
        if (currentLogic == null)
            return;
        currentLogic.OnClick(this);
    }
}
