﻿using UnityEngine;
using System.Collections;

public class Area : MonoBehaviour, IDropableArea
{
    public AreaLogic al;

    public void OndDrop()
    {
        al.Execute();
    }
}
