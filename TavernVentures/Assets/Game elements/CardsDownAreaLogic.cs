﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Areas/Cards down when holding card")]
public class CardsDownAreaLogic : AreaLogic
{
    public CardVariable card;
    public SO.TransformVariable cardsDownArea;
    public GameElementsLogic cardDownLogic;

    public override void Execute()
    {
        if (card.value == null)
            return;

        if (playerCanPlayCard(Settings.GetPlayersManager().GetCurrentPlayer(), Settings.GetGameManager().turnColor, card))
        {
            card.value.currentLogic = cardDownLogic;
            Settings.GetServerConnection().SendRequest("PLAY " + card.value.cardDisplay.card.value);
            Settings.GetPlayersManager().GetCurrentPlayer().canUseCards = false;
            Settings.GetPlayersManager().GetCurrentPlayer().handCards.Remove(card.value);
            GameObject.Destroy(card.value.gameObject);

            List<PlayerHolder> singlePlayer = new List<PlayerHolder>();
            singlePlayer.Add(Settings.GetPlayersManager().GetCurrentPlayer());
            Settings.GetGameManager().StopTimerOnClick(singlePlayer);
        }
    }

    public static bool playerCanPlayCard(PlayerHolder player, CardType color, CardVariable c)
    {
        if (player.canUseCards)
        {
            if (color == CardType.Colorless || c.value.cardDisplay.card.type == CardType.Colorless || c.value.cardDisplay.card.type == color || !player.hasColor(color))
            {
                return true;
            }
        }
        return false;
    }
}
