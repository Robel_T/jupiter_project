﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Game Elements/Hand card")]
public class HandCard : GameElementsLogic
{
    public SO.GameEvent onCurrentCardSelected;
    public CardVariable currentCard;
    public State holdingCard;

    public override void OnClick(CardInstance inst)
    {
        currentCard.Set(inst);
        Settings.GetGameManager().SetState(holdingCard);
        onCurrentCardSelected.Raise();
    }
}
