﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BetClick : MonoBehaviour
{
    public Button bet;
    public GameObject betChoices;

    void Start()
    {
        bet.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {
        Settings.GetServerConnection()
            .SendRequest("BET " + bet.GetComponentInChildren<Text>().text);
        Settings.GetGameManager().GetBetPhase().ClearBetChoices();
        Settings.GetGameManager().StopTimerOnClick(Settings.GetPlayersManager().allPlayers);
    }
}
