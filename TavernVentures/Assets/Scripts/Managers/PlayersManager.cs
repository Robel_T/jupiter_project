﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersManager : ScriptableObject
{
    public PlayerHolder currentPlayer;
    public List<PlayerHolder> allPlayers = new List<PlayerHolder>();

    public void createLocalPlayer(string name)
    {
        PlayerHolder ph = createPlayer(name, Settings.GetResourcesManager().GetIcon(0));
        currentPlayer = ph;
    }


    public PlayerHolder createPlayer(string name, Sprite icon, int id = 6)
    {
        PlayerHolder ph = new PlayerHolder(name, icon, id);
        ph.statsUI = new PlayerStatsUI(ph);
        allPlayers.Add(ph);
        return ph;
    }

    public PlayerHolder GetCurrentPlayer()
    {
        return currentPlayer;
    }

    public PlayerHolder GetPlayerById(int id)
    {
        foreach (PlayerHolder p in allPlayers)
        {
            if (p.id == id)
            {
                return p;
            }
        }
        return null;
    }

    public bool doesPlayerExist(int id)
    {
        foreach (PlayerHolder p in allPlayers)
        {
            if (p.id == id)
            {
                return true;
            }
        }
        return false;
    }
}
