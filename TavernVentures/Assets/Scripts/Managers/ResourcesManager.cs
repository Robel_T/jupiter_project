﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using System.Runtime.InteropServices;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Managers/Resources manager")]
public class ResourcesManager : ScriptableObject
{
    public Card[] allCards;
    public List<Sprite> icons;
    public Dictionary<string, Card> cardsDict = new Dictionary<string, Card>();

    public void Init()
    {
        cardsDict.Clear();
        for (int i = 0; i < allCards.Length; i++)
        {
            cardsDict.Add(allCards[i].name, allCards[i]);
        }
    }

    internal Sprite GetIcon(object p)
    {
        throw new NotImplementedException();
    }

    public Card GetCardInstance(string id)  
    {
        Card originalCard = GetCard(id);
        if (originalCard == null)
            return null;

        Card newInst = Instantiate(originalCard);
        newInst.name = originalCard.name;
        return newInst;
    }

    public Card GetCard(string id)
    {
        Card result = null;
        cardsDict.TryGetValue(id, out result);
        return result;
    }

    public Sprite GetIcon(int i)
    {
        if(i < icons.Count)
        {
            return icons[i];
        }
        return null;
    }

    public Texture2D LoadTexture(string FilePath)
    {
        //https://forum.unity.com/threads/generating-sprites-dynamically-from-png-or-jpeg-files-in-c.343735/
        Texture2D Tex2D;
        byte[] FileData;

        FileData = File.ReadAllBytes(FilePath);
        Tex2D = new Texture2D(2, 2);
        Tex2D.LoadImage(FileData);
        return Tex2D;
    }
}
