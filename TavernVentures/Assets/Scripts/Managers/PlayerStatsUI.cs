﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerStatsUI : MonoBehaviour
{
    public PlayerHolder player;
    public GameObject portait;
    public Image leaderIcon;
    public Image playerIcon;
    public Image playerTimer;
    public Text playerName;
    public Text playerPoints;
    public GameObject playerBets;
    public Text playerCurrentBets;
    public Text playerMaxBets;
    public GameObject playerLastPlayedCard;
    public GameObject highlight;
    public Image excludedIcon;
    public bool stopTimer = false;

    public PlayerStatsUI(PlayerHolder player)
    {
        this.player = player;
    }

    public void DisplayPortrait(bool display)
    {
        portait.SetActive(display);
    }

    public void DisplayLeaderIcon(bool display)
    {
        leaderIcon.gameObject.SetActive(display);
    }

    public void DisplayBets(bool display)
    {
        Color color = new Color32(1, 184, 255, 255);
        playerBets.GetComponent<Text>().color = color;
        foreach (Text text in playerBets.GetComponentsInChildren<Text>())
        {
            text.color = color;
        }
        playerBets.SetActive(display);
    }

    public void DisplayLastPlayedCard(bool display)
    {
        playerLastPlayedCard.SetActive(display);
    }

    public void DisplayLeadingCard(bool display)
    {
        highlight.SetActive(display);
    }

    public void DisplayExcludedIcon(bool display)
    {
        excludedIcon.gameObject.SetActive(display);
    }

    public void UpdateAll()
    {
        UpdatePlayerName();
        UpdatePlayerPoints();
        UpdatePlayerCurrentBets();
        UpdatePlayerMaxBets();
        UpdatePlayerLastPlayedCard(null);
    }

    public void UpdatePlayerName()
    {
        playerName.text = player.name;
        playerIcon.sprite = player.icon;
    }

    public void UpdatePlayerTimer(float amount)
    {
        if (stopTimer)
        {
            playerTimer.fillAmount = 0;
        }
        else
        {
            playerTimer.fillAmount = amount;
        }
    }

    public void UpdatePlayerPoints()
    {
        playerPoints.text = player.points.ToString();
    }

    public void UpdatePlayerCurrentBets()
    {
        if (player.currentBets == player.maxBets)
        {
            Color color = new Color32(0, 255, 0, 255);
            playerBets.GetComponent<Text>().color = color;
            foreach (Text text in playerBets.GetComponentsInChildren<Text>())
            {
                text.color = color;
            }
        }

        if (player.currentBets > player.maxBets)
        {
            Color color = new Color32(255, 0, 0, 255);
            playerBets.GetComponent<Text>().color = color;
            foreach (Text text in playerBets.GetComponentsInChildren<Text>())
            {
                text.color = color;
            }
        }

        playerCurrentBets.text = player.currentBets.ToString();
    }

    public void UpdatePlayerMaxBets()
    {
        playerMaxBets.text = player.maxBets.ToString();
    }

    public void UpdatePlayerLastPlayedCard(Sprite artwork)
    {
        playerLastPlayedCard.GetComponentsInChildren<Image>()[1].sprite = artwork;
    }
}
