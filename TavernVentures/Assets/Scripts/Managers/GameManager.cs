﻿using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using System.Threading;

[CreateAssetMenu(menuName = "Managers/Game manager")]
public class GameManager : MonoBehaviour
{
    public State currentState;
    public State playerControlState;

    public int roundIndex;
    public int turnIndex;
    public PlayerStatsUI[] statsUI;
    public Hand myHand = new Hand();
    public Bets bets = new Bets();
    public Leader oldLeader = new Leader();
    public Leader newLeader = new Leader();
    public Points point = new Points();
    public NewPlay play = new NewPlay();
    public int nbPlayableCards = 60; // 8 Doors + 13 Blue + 13 Red + 13 Yellow + 13 Black

    public GameObject cardPrefab;
    public GameObject betChoicePrefab;
    public GameObject turnColorCircle;
    public GameObject scoreText;
    public SO.GameEvent OnRoundChange;
    public SO.GameEvent OnPhaseChange;
    public SO.GameEvent OnTurnChange;
    public SO.GameEvent OnPlayerChange;
    public SO.StringVariable roundText;
    public SO.StringVariable phaseText;
    public SO.StringVariable turnText;

    public SO.TransformVariable betGrid;
    public SO.TransformVariable handGrid;
    public SO.TransformVariable downGrid;
    [System.NonSerialized]
    public List<CardInstance> cardsDown = new List<CardInstance>();
    public GameElementsLogic handLogic;
    public GameElementsLogic downLogic;
    public GameObject cardDownGrid;

    private BetPhase betPhase;
    private PlayCardPhase playCardPhase;

    public GameObject scoreCard;
    public GameObject confirmQuitDialog;

    private bool end = false;
    private bool newCards = false;
    private bool newBets = false;
    private bool newPlay = false;
    private bool newToken = false;
    private bool newPoint = false;
    public int newExcluded = -1;
    private bool newTurnwinner = false;
    public int turnwinner = 0;

    public CardType turnColor = CardType.Colorless;
    public Color circleColor = new Color32(0, 0, 0, 0);

    private float maxTime;
    private float activeTime;
    private bool isTimerStarted, isBetPhase;
    private int playingPlayer;
    private int lastFirstPlayer;

    public void Start()
    {
        SetState(playerControlState);
        SetupUI();
        roundIndex = 1;
        roundText.value = "Manche " + roundIndex;
        turnIndex = 1;
        turnText.value = "Tour " + turnIndex;
        Settings.SetGameManager(this);
        OnRoundChange.Raise();
        playingPlayer = 0;
        lastFirstPlayer = 0;
    }

    public void Update()
    {
        if (currentState != null)
            currentState.Tick(Time.deltaTime);

        // Player played a card
        if (newPlay)
        {
            newPlay = false;
            for (int i = 0; i < Settings.GetPlayersManager().allPlayers.Count; ++i)
            {
                playingPlayer = (playingPlayer + 1) % Settings.GetPlayersManager().allPlayers.Count;
                if (!Settings.GetPlayersManager().GetPlayerById(playingPlayer).isExluded)
                {
                    break;
                }
            }

            UpdatePhaseText();
            UpdateAfterPlay();
        }

        // End turn
        if (newTurnwinner)
        {
            newTurnwinner = false;
            IncrementWinnerDoneBet();
            DisplayLastPlayedCardUI(false);
            ClearCardsDownArea();
            phaseText.value = Settings.GetPlayersManager().GetPlayerById(turnwinner).name + " remporte le tour";
            playingPlayer = turnwinner;
            OnPlayerChange.Raise();
            turnText.value = "Tour " + (++turnIndex);
            OnTurnChange.Raise();
        }

        // End round
        if (newPoint)
        {
            newPoint = false;
            DisplayBetsUI(false);
            UpdatePlayersPoints();
            phaseText.value = "Attribution des points";
            OnPhaseChange.Raise();
            roundText.value = "Manche " + (++roundIndex);
            turnIndex = 1;
            turnText.value = "Tour " + turnIndex;
            OnRoundChange.Raise();
        }

        // Start round (bet phase)
        if (newCards)
        {
            newCards = false;
            DrawCards();
            isBetPhase = true;
            betPhase = new BetPhase();
            betPhase.OnStartPhase();
            phaseText.value = "Faites vos paris";
            OnPhaseChange.Raise();
            for (int i = 0; i < Settings.GetPlayersManager().allPlayers.Count; ++i)
            {
                lastFirstPlayer = (lastFirstPlayer + 1) % Settings.GetPlayersManager().allPlayers.Count;
                if (!Settings.GetPlayersManager().GetPlayerById(lastFirstPlayer).isExluded)
                {
                    break;
                }
            }
            playingPlayer = lastFirstPlayer;
        }

        // End bet phase
        if (newBets)
        {
            newBets = false;
            isBetPhase = false;
            UpdatePlayersBets();
            DisplayBetsUI(true);
        }

        // Start player's turn
        if (newToken)
        {
            newToken = false;
            UpdatePhaseText();
            playCardPhase = new PlayCardPhase();
            playCardPhase.OnStartPhase();
        }

        // Excluded players
        if (newExcluded != -1)
        {
            Settings.GetPlayersManager().GetPlayerById(newExcluded).isExluded = true;
            Settings.GetPlayersManager().GetPlayerById(newExcluded).statsUI.DisplayExcludedIcon(true);
            newExcluded = -1;
        }

        // End game
        if (end)
        {
            DisplayResult();
        }

        // Update timers
        if (isTimerStarted)
        {
            if (isBetPhase)
            {
                UpdateTimer(Settings.GetPlayersManager().allPlayers);
            } else
            {

                UpdateTimer(Settings.GetPlayersManager().allPlayers); // TODO : à faire uniquement pour le joueur concerné durant son tour
            }
        }
    }

    public void SetupUI()
    {
        for (int i = 0; i < Settings.GetPlayersManager().allPlayers.Count; i++)
        {
            statsUI[i].player = Settings.GetPlayersManager().GetPlayerById(i);
            statsUI[i].player.LoadPlayerOnStatsUI();
            statsUI[i].playerName.text = Settings.GetPlayersManager().GetPlayerById(i).name;
            statsUI[i].playerIcon.sprite = Settings.GetPlayersManager().GetPlayerById(i).icon;
            statsUI[i].DisplayPortrait(true);
            statsUI[i].DisplayBets(false);
            statsUI[i].DisplayLastPlayedCard(false);
            Settings.GetPlayersManager().GetPlayerById(i).statsUI = statsUI[i];
        }
    }

    public void SetState(State state)
    {
        currentState = state;
    }

    public void DrawCards()
    {
        ResourcesManager rm = Settings.GetResourcesManager();
        foreach (ProtoCard card in myHand.hand) {
            string cardId = card.card;
            GameObject go = Instantiate(cardPrefab) as GameObject;
            CardDisplay cd = go.GetComponent<CardDisplay>();
            cd.LoadCard(rm.GetCardInstance(cardId));
            CardInstance inst = go.GetComponent<CardInstance>();
            inst.currentLogic = handLogic;
            Settings.SetParentForHandCard(go.transform, handGrid.value.transform);
            Settings.GetPlayersManager().GetCurrentPlayer().handCards.Add(inst);
        }
    }

    public void MakeHand(string cards)
    {
        myHand = JsonUtility.FromJson<Hand>(cards);
        newCards = true;
    }

    public void ClearCardsDownArea()
    {
        foreach (CardInstance inst in cardsDown)
        {
            inst.gameObject.SetActive(false);
        }
        Settings.GetPlayersManager().GetPlayerById(newLeader.leader.player).statsUI.highlight.SetActive(false);
        cardsDown.Clear();
        foreach (Transform child in cardDownGrid.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        turnColor = CardType.Colorless;
        circleColor = new Color32(0, 0, 0, 0);
        turnColorCircle.GetComponent<Image>().color = circleColor;
    }

    [Serializable]
    public class Hand
    {
        public ProtoCard[] hand;
    }

    [Serializable]
    public class ProtoCard
    {
        public string card;
    }

    public void ReceivedPlayersBets(string bets)
    {
        this.bets = JsonUtility.FromJson<Bets>(bets);
        newBets = true;
    }

    [Serializable]
    public class Bets
    {
        public Bet[] bets;
    }

    [Serializable]
    public class Bet
    {
        public int id;
        public int bet;
    }

    private void UpdatePlayersBets()
    {
        foreach(PlayerHolder p in Settings.GetPlayersManager().allPlayers)
        {
            foreach (Bet b in bets.bets)
            {
                if(b.id == p.id)
                {
                    p.SetMaxBets(b.bet);
                    break;
                }
            }

            if (p.statsUI != null)
            {
                p.statsUI.UpdatePlayerMaxBets();
            }
        }
    }

    public BetPhase GetBetPhase()
    {
        return betPhase;
    }

    private void UpdateAfterPlay()
    {
        PlayerHolder p = Settings.GetPlayersManager().GetPlayerById(play.newPlay.player);
        p.statsUI.UpdatePlayerLastPlayedCard(Settings.GetResourcesManager().GetCardInstance(play.newPlay.card).artwork);
        p.statsUI.DisplayLastPlayedCard(true);
        turnColorCircle.GetComponent<Image>().color = circleColor;

        GameObject go = Instantiate(cardPrefab) as GameObject;
        CardDisplay cd = go.GetComponent<CardDisplay>();
        cd.LoadCard(Settings.GetResourcesManager().GetCardInstance(play.newPlay.card));
        CardInstance inst = go.GetComponent<CardInstance>();
        inst.currentLogic = downLogic;
        cardsDown.Add(inst);
        Settings.DropCard(inst.transform, cardDownGrid.transform, inst);

        if (oldLeader.leader != null) {
            Settings.GetPlayersManager().GetPlayerById(oldLeader.leader.player).statsUI.DisplayLeadingCard(false);
        }
        Settings.GetPlayersManager().GetPlayerById(newLeader.leader.player).statsUI.DisplayLeadingCard(true);

    }

    public void ReceivedPlay(string newPlay, string actualLeader, string color)
    {
        if (turnColor == CardType.Colorless)
        {
            switch (color)
            {
                case "BLACK":
                    turnColor = CardType.Black;
                    circleColor = new Color32(0, 0, 0, 150);
                    break;

                case "BLUE":
                    turnColor = CardType.Blue;
                    circleColor = new Color32(127, 172, 255, 150);
                    break;

                case "RED":
                    turnColor = CardType.Red;
                    circleColor = new Color32(239, 92, 92, 150);
                    break;

                case "YELLOW":
                    turnColor = CardType.Yellow;
                    circleColor = new Color32(255, 192, 71, 150);
                    break;
            }
        }
        
        play = JsonUtility.FromJson<NewPlay>(newPlay);
        oldLeader = newLeader;
        newLeader = JsonUtility.FromJson<Leader>(actualLeader);
        this.newPlay = true;
    }

    [Serializable]
    public class NewPlay
    {
        public Play newPlay;
    }

    [Serializable]
    public class Leader
    {
        public Play leader;
    }

    [Serializable]
    public class Play
    {
        public String card;
        public int player;
    }

    public void AllowUserToPLay()
    {
        newToken = true;
    }

    public void IncrementWinnerDoneBet()
    {
        Settings.GetPlayersManager().GetPlayerById(turnwinner).WinTurn();
    }

    public void ReceivedTurnwinner(int playerId)
    {
        turnwinner = playerId;
        newTurnwinner = true;
    }

    public void ReceivedExcluded(int playerId)
    {
        newExcluded = playerId;
    }

    public void UpdatePlayersPoints()
    {
        List<int> leaders = new List<int>();
        int maxPoint = -550;
        foreach(PlayerPoint p in point.playerPoints)
        {
            if(maxPoint < p.points)
            {
                leaders.Clear();
                leaders.Add(p.id);
                maxPoint = p.points;
            }else if(maxPoint == p.points)
            {
                leaders.Add(p.id);
            }
            Settings.GetPlayersManager().GetPlayerById(p.id).SetPoints(p.points);
            Settings.GetPlayersManager().GetPlayerById(p.id).statsUI.DisplayLeaderIcon(false);
        }

        foreach(int i in leaders)
        {
            Settings.GetPlayersManager().GetPlayerById(i).statsUI.DisplayLeaderIcon(true);
        }
    }

    public void ReceivedPoints(string points)
    {
        point = JsonUtility.FromJson<Points>(points);
        newPoint = true;
    }

    public void DisplayBetsUI(bool display)
    {
        foreach (PlayerHolder p in Settings.GetPlayersManager().allPlayers)
        {
            p.statsUI.DisplayBets(display);
        }
    }

    public void DisplayLastPlayedCardUI(bool display)
    {
        foreach (PlayerHolder p in Settings.GetPlayersManager().allPlayers)
        {
            p.statsUI.DisplayLastPlayedCard(display);
        }
    }

    [Serializable]
    public class Points
    {
        public PlayerPoint[] playerPoints;
    }

    [Serializable]
    public class PlayerPoint
    {
        public int id;
        public int points;
    }

    public void EndGame()
    {
        end = true;
    }

    public void DisplayResult()
    {
        scoreCard.SetActive(true);
    }

    public void HideResult()
    {
        scoreCard.SetActive(false);
    }

    public void DisplayQuit()
    {
        confirmQuitDialog.SetActive(true);
    }

    public void HideQuit()
    {
        confirmQuitDialog.SetActive(false);
    }

    public void InitTimer(float time, List<PlayerHolder> players)
    {
        maxTime = time;
        activeTime = 0f;
        isTimerStarted = true;
        foreach (PlayerHolder p in players)
        {
            if (p.statsUI != null)
            {
                p.statsUI.UpdatePlayerTimer(0f);
            }
        }
    }

    public void UpdateTimer(List<PlayerHolder> players)
    {
        activeTime += Time.deltaTime;
        float fillPercentage = activeTime / maxTime;
        foreach (PlayerHolder p in players)
        {
            if (p.statsUI != null)
            {
                p.statsUI.UpdatePlayerTimer(fillPercentage);
            }
        }

        if (activeTime >= maxTime)
        {
            StopTimer(players);
        }
        else if (allTimersStopped(players))
        {
            StopTimerOnClick(players);
        }
    }

    public void StopTimer(List<PlayerHolder> players)
    {
        isTimerStarted = false;
        foreach (PlayerHolder p in players)
        {
            if (p.statsUI != null)
            {
                p.statsUI.UpdatePlayerTimer(0f);
                p.statsUI.stopTimer = false;
                if (p.id == Settings.GetPlayersManager().GetCurrentPlayer().id)
                {
                    if (isBetPhase)
                    {
                        Settings.GetServerConnection().SendRequest("BET 0");
                        betPhase.ClearBetChoices();
                    }
                    else
                    {
                        foreach (CardInstance c in Settings.GetPlayersManager().GetCurrentPlayer().handCards)
                        {
                            CardVariable card = new CardVariable();
                            card.Set(c);
                            if (CardsDownAreaLogic.playerCanPlayCard(Settings.GetPlayersManager().currentPlayer, Settings.GetGameManager().turnColor, card))
                            {
                                Settings.GetServerConnection().SendRequest("PLAY " + card.value.cardDisplay.card.value);
                                Settings.GetPlayersManager().GetCurrentPlayer().handCards.Remove(card.value);
                                GameObject.Destroy(card.value.gameObject);
                                break;
                            }
                        }
                        p.canUseCards = false;
                    }
                }
            }
        }
    }

    public void StopTimerOnClick(List<PlayerHolder> players)
    {
        isTimerStarted = false;
        foreach (PlayerHolder p in players)
        {
            if (p.statsUI != null)
            {
                p.statsUI.UpdatePlayerTimer(0f);
                p.statsUI.stopTimer = false;
            }
        }
    }

    public bool allTimersStopped(List<PlayerHolder> players)
    {
        foreach (PlayerHolder p in players)
        {
            if (p.statsUI != null)
            {
                if (!p.statsUI.stopTimer)
                {
                    return false;
                }
            }
        }
        return true;
    }

    public void UpdatePhaseText()
    {
        phaseText.value = "Au tour de " + Settings.GetPlayersManager().GetPlayerById(playingPlayer).name;
        OnPlayerChange.Raise();
    }
}
