﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public static class Settings
{
    private static JoinAParty joinAParty;
    private static InitAParty initAParty;
    private static WaitingRoomManager waitingRoomManager;
    private static GameManager gameManager;
    private static ServerConnection serverConnection;
    private static ResourcesManager resourcesManager;
    private static PlayersManager playersManager;

    public static ResourcesManager GetResourcesManager()
    {
        if (resourcesManager == null)
        {
            resourcesManager = Resources.Load("ResourcesManager") as ResourcesManager;
            resourcesManager.Init();
        }

        return resourcesManager;
    }

    public static ServerConnection GetServerConnection()
    {
        if (serverConnection == null)
        {
            serverConnection = new ServerConnection();//Resources.Load("ServerConnection") as ServerConnection;
        }

        return serverConnection;
    }

    public static PlayersManager GetPlayersManager()
    {
        if (playersManager == null)
        {
            playersManager = new PlayersManager();//Resources.Load("GameManager") as GameManager;
        }

        return playersManager;
    }

    public static List<RaycastResult> GetUIObjs()
    {
        PointerEventData ped = new PointerEventData(EventSystem.current)
        {
            position = Input.mousePosition
        };

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(ped, results);

        return results;
    }

    public static void DropCard(Transform c, Transform p, CardInstance inst)
    {
        SetParentForCardDown(c, p);
    }

    public static void SetParentForBetChoice(Transform c, Transform p)
    {
        c.SetParent(p);
        c.localPosition = Vector3.zero;
        c.localEulerAngles = Vector3.zero;
        c.localScale = Vector3.one;
    }

    public static void SetParentForHandCard(Transform c, Transform p)
    {
        c.SetParent(p);
        c.localPosition = Vector3.zero;
        c.localEulerAngles = Vector3.zero;
        c.localScale = Vector3.one;
    }

    public static void SetParentForCardDown(Transform c, Transform p)
    {
        c.SetParent(p);
        c.localPosition = Vector3.zero;
        c.localEulerAngles = new Vector3(0, 0, Random.Range(-15.0f, 15.0f));
        c.localScale = Vector3.one;
    }

    public static void SetJoinAParty(JoinAParty jap)
    {
        joinAParty = jap;
    }

    public static void SetInitAParty(InitAParty iap)
    {
        initAParty = iap;
    }

    public static void ShowWaitingRoom()
    {
        while (true)
        {
            if(initAParty != null)
            {
                initAParty.InitReceived();
                break ;
            }
            if(joinAParty != null)
            {
                joinAParty.InitReceived();
                break;
            }
        }
    }

    public static JoinAParty GetJoinAParty()
    {
        while (joinAParty == null) {}
        return joinAParty;
    }

    public static WaitingRoomManager GetWaitingRoomManager()
    {
        while (waitingRoomManager == null) {}
        return waitingRoomManager;
    }

    public static InitAParty GetInitAParty()
    {
        while (initAParty == null) {}
        return initAParty;
    }

    public static void SetWaitingRoomManager(WaitingRoomManager wrm)
    {
        waitingRoomManager = wrm;
    }
    public static GameManager GetGameManager()
    {
        while (gameManager == null) {}
        return gameManager;
    }

    public static void SetGameManager(GameManager gm)
    {
        gameManager = gm;
    }

}