﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WaitingRoomManager : MonoBehaviour
{
    public Text roomId;
    public GameObject view;
    public Button begin;
    public Text beginGame;
    public List<GameObject> players;
    public List<Sprite> playersIcons;

    public string id = "";
    public PlayersList pl;
    private bool gameBegin = false;

    public void Start()
    {
        playersIcons = Settings.GetResourcesManager().icons;
        //RandomizeIcons(playersIcons);
        Settings.SetWaitingRoomManager(this);
        pl = new PlayersList();
        pl.players = new Protoplayer[0];
        beginGame.text = "";
        begin.GetComponent<Button>().onClick.AddListener(TaskOnClick);
        foreach(GameObject go in players)
        {
            go.SetActive(false);
        }
    }

    public void Update()
    {
        this.roomId.GetComponent<Text>().text = id;
        int nbPlayers = pl.players.Length;
        DisplayPortraits.nbPlayers = nbPlayers;
        if((nbPlayers > 1) && (Settings.GetPlayersManager().currentPlayer.id == 0))
        {
            if (nbPlayers == Convert.ToInt32(Settings.GetInitAParty().nbPlayer.value))
            {
                begin.gameObject.SetActive(false);
                beginGame.text = "La partie va commencer...";
            }
            else
            {
                begin.gameObject.SetActive(true);
            }

        }
        else
        {
            begin.gameObject.SetActive(false);
        }
        float space = 250;
        float x = (Screen.width - space * (nbPlayers - 1)) / 2;
        for(int i = 0; i < Settings.GetPlayersManager().allPlayers.Count; ++i)
        {
            players[i].gameObject.transform.position = new Vector3(x, players[i].gameObject.transform.position.y);
            players[i].GetComponent<Image>().sprite = Settings.GetPlayersManager().GetPlayerById(i).icon;
            players[i].GetComponentInChildren<Text>().text = Settings.GetPlayersManager().GetPlayerById(i).name;
            players[i].SetActive(true);
            x += space;
        }
        if (gameBegin)
        { 
            SceneManager.LoadScene(1);
        }
    }

    public void ManageRoom(string roomId, string players)
    {
        if (id == "") 
        {
            id = roomId;
        }
        UpdatePlayers(players);
        
    }

    public void InitGame(string players)
    {
        UpdatePlayers(players);
        gameBegin = true;
    }

    private void UpdatePlayers(string players)
    {
        pl = JsonUtility.FromJson<PlayersList>(players);
        for (int i = 0; i < pl.players.Length; i++)
        {
            Protoplayer p = pl.players[i];
            if (!Settings.GetPlayersManager().doesPlayerExist(p.id))
            {
                Settings.GetPlayersManager().createPlayer(p.name, playersIcons[i], p.id);
            }
        }
    }

    [Serializable]
    public class PlayersList
    {
        public Protoplayer[] players;
    }

    [Serializable]
    public class Protoplayer
    {
        public int id;
        public string name;
    }

    void TaskOnClick()
    {
        Settings.GetServerConnection().SendRequest("BEGIN");
    }

    public void RandomizeIcons(List<Sprite> icons)
    {
        int count = icons.Count;
        int last = count - 1;
        for (int i = 0; i < last; i++)
        {
            int r = UnityEngine.Random.Range(i, count);
            Sprite tmp = icons[i];
            icons[i] = icons[r];
            icons[r] = tmp;
        }
    }
}
