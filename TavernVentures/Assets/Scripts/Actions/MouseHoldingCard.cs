﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[CreateAssetMenu(menuName = "Actions/Mouse holding card")]
public class MouseHoldingCard : Action
{
    public CardVariable currentCard;
    public State playerControlState;
    public SO.GameEvent onPlayerControlState;

    public override void Execute(float d)
    {
        bool mouseIsDown = Input.GetMouseButton(0);

        if (!mouseIsDown)
        {
            List<RaycastResult> results = Settings.GetUIObjs();

            foreach (RaycastResult r in results)
            {
                Area area = r.gameObject.GetComponentInParent<Area>();
                if (area != null)
                {
                    area.OndDrop();
                    break;
                }
            }

            if (currentCard.value != null)
            {
                currentCard.value.gameObject.SetActive(true);
                currentCard.value = null;
            }

            Settings.GetGameManager().SetState(playerControlState);
            onPlayerControlState.Raise();
            return;
        }
    }
}
