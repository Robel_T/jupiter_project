﻿using UnityEngine;
using UnityEngine.UI;

public class MainTimer : MonoBehaviour
{
    private float maxTime;
    private float activeTime;
    private bool isStarted, isFinished;
    public Image fillImage;
    public Text timeLeft;

    public void Init(float time)
    {
        maxTime = time;
        activeTime = 0f;
        isStarted = true;
        isFinished = false;
        fillImage.fillAmount = 1f;
        timeLeft.text = maxTime.ToString();
    }

    public void Update()
    {
        if (isStarted && !isFinished)
        {
            activeTime += Time.deltaTime;
            float percentage = activeTime / maxTime;
            fillImage.fillAmount = 1 - percentage;
            timeLeft.text = (Mathf.Round((maxTime - activeTime) * 10f) / 10f).ToString();

            if (activeTime >= maxTime)
            {
                Stop();
            }
        }
    }

    public void Stop()
    {
        fillImage.fillAmount = 0f;
        timeLeft.text = "";
        isFinished = true;
    }
}