﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Rounds/Play card phase")]
public class PlayCardPhase : Phase
{
    public override void OnStartPhase()
    {
        Settings.GetGameManager().SetState(Settings.GetGameManager().playerControlState);
        List<PlayerHolder> singlePlayer = new List<PlayerHolder>();
        singlePlayer.Add(Settings.GetPlayersManager().GetCurrentPlayer());
        int phaseTime = 10 + (Settings.GetGameManager().roundIndex - Settings.GetGameManager().turnIndex); // TODO : formule du temps max à changer ?
        Settings.GetGameManager().InitTimer(phaseTime, singlePlayer);
        Settings.GetPlayersManager().GetCurrentPlayer().canUseCards = true;

    }
}
