﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

[CreateAssetMenu(menuName = "Rounds/Bet phase")]
public class BetPhase : Phase
{
    public override void OnStartPhase()
    {
        Settings.GetGameManager().SetState(null);
        InitBetChoices();

        int phaseTime = 10 + Settings.GetGameManager().roundIndex; // TODO : formule du temps max à changer ?
        Settings.GetGameManager().InitTimer(phaseTime, Settings.GetPlayersManager().allPlayers);
    }

    public void InitBetChoices()
    {
        for (int i = 0; i < Settings.GetGameManager().roundIndex + 1; i++)
        {
            GameObject go = Instantiate(Settings.GetGameManager().betChoicePrefab) as GameObject;
            go.GetComponentInChildren<Text>().text = i.ToString();
            Settings.SetParentForBetChoice(go.transform, Settings.GetGameManager().betGrid.value.transform);
            Settings.GetPlayersManager().GetCurrentPlayer().betChoices.Add(go);
        }
    }

    public void ClearBetChoices()
    {
        for (int i = 0; i < Settings.GetPlayersManager().allPlayers.Count; i++)
        {
            foreach (GameObject go in Settings.GetPlayersManager().allPlayers[i].betChoices)
            {
                go.SetActive(false);
            }
            Settings.GetPlayersManager().allPlayers[i].betChoices.Clear();
        }
    }
}
