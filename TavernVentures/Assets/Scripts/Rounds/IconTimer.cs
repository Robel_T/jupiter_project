﻿using UnityEngine;
using UnityEngine.UI;

public class IconTimer : MonoBehaviour
{
    private float maxTime;
    private float activeTime;
    private bool isStarted, isFinished;
    public Image fillImage;

    public void Init(float time)
    {
        maxTime = time;
        activeTime = 0f;
        isStarted = true;
        isFinished = false;
        fillImage.fillAmount = 0f;
    }

    public void Update()
    {
        if (isStarted && !isFinished)
        {
            activeTime += Time.deltaTime;
            float fillPercentage = activeTime / maxTime;
            fillImage.fillAmount = fillPercentage;

            if (activeTime >= maxTime)
            {
                Stop();
            }
        }
    }

    public void Stop()
    {
        fillImage.fillAmount = 0f;
        isFinished = true;
    }
}