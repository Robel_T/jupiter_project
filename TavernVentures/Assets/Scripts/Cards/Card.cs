﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CardType
{
    Colorless,
    Red,
    Blue,
    Yellow,
    Black
}

[CreateAssetMenu(fileName = "New card", menuName = "Card")]
public class Card : ScriptableObject
{
    public CardType type;
    public string value;
    public Sprite artwork;

    public Card(CardType type, string value, Sprite artwork)
    {
        this.type = type;
        this.value = value;
        this.artwork = artwork;
    }
}
