﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class populateGrid : MonoBehaviour
{

    public GameObject scoreText;

    // Start is called before the first frame update
    void Start()
    {
        Populate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Populate()
    {
        GameObject player;
        GameObject score;
        GameObject rank;

        List<string> ranking = playerRanks();

        foreach (PlayerHolder p in Settings.GetPlayersManager().allPlayers)
        {
            player = (GameObject)Instantiate(scoreText, transform);
            score = (GameObject)Instantiate(scoreText, transform);
            rank = (GameObject)Instantiate(scoreText, transform);

            player.GetComponentInChildren<Text>().text = p.name;

            score.GetComponentInChildren<Text>().text = p.points.ToString();

            rank.GetComponentInChildren<Text>().text = (ranking.FindIndex(x => x.Equals(p.name)) + 1).ToString();
        }
    }

    public List<string> playerRanks()
    {
        List<PlayerHolder> SortedList = Settings.GetPlayersManager().allPlayers;
        SortedList.Sort((x, y) => y.points.CompareTo(x.points));

        List<string> pr = new List<string>();
        foreach(PlayerHolder p in SortedList)
        {
            pr.Add(p.name);
        }
        return pr;
    }
}
