﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class buttonPush : MonoBehaviour
{
    public void homeButton()
    {
        Settings.GetGameManager().DisplayQuit();
    }

    public void yesQuitButton()
    {
        Settings.GetServerConnection().Disconnect();
        SceneManager.LoadScene(sceneName: "Menu");
        Settings.GetPlayersManager().allPlayers.Clear();
    }

    public void noQuitButton()
    {
        Settings.GetGameManager().HideQuit();
    }
}
