﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayPortraits : MonoBehaviour
{
    public List<Image> portraits;
    public static int nbPlayers = 1;

    private void Start()
    {
        AlignPortraits();
    }

    private void Update()
    {
        AlignPortraits();
    }

    public void AlignPortraits()
    {
        float space = 250;
        float x = (Screen.width - space * (nbPlayers - 1)) / 2;

        for (int i = 0; i < nbPlayers; i++)
        {
            portraits[i].gameObject.SetActive(true);
            portraits[i].gameObject.transform.position = new Vector3(x, 450);
            x += space;
        }
    }
}
