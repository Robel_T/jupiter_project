﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayPlayerInfos : MonoBehaviour
{
    public List<Image> portraits;

    public void DisplayInfos(int nbPlayers, Text textUi)
    {
        for (int i = 0; i < nbPlayers; i++)
        {
            if (portraits[0].transform.GetChild(1).gameObject.gameObject.GetComponent<Text>().text == "En attente...")
            {
                //portraits[i].gameObject.GetComponent<Image>().sprite = ...;
                portraits[i].transform.GetChild(1).gameObject.gameObject.GetComponent<Text>().text = textUi.text;
                break;
            }
        }
    }
}
