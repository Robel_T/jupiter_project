﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitGame : MonoBehaviour
{
    public Button send;

    void Start()
    {
        Button btn = send.GetComponent<Button>();
        btn.onClick.AddListener(Exit);
    }

    void Exit()
    {
        Application.Quit();
    }
}
