﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentSelectedCard : MonoBehaviour
{
    public CardVariable currentCard;
    public CardDisplay cardDisplay;
    Transform mTransform;

    public void LoadCard()
    {
        if (currentCard.value == null)
            return;

        currentCard.value.gameObject.SetActive(false);
        cardDisplay.LoadCard(currentCard.value.cardDisplay.card);
        cardDisplay.gameObject.SetActive(true);
    }

    public void CloseCard()
    {
        cardDisplay.gameObject.SetActive(false);
    }

    public void Start()
    {
        mTransform = this.transform;
        CloseCard();
    }

    public void Update()
    {
        mTransform.position = Input.mousePosition;
    }
}
