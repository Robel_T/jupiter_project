﻿using System;
using System.Linq;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.UI;

public class InitAParty : MonoBehaviour
{
    public Text nameErrorMsg;
    public Button send;
    public Text user;
    public Slider nbPlayer;
    public GameObject waitingRoomPanel;
    public bool initReceived = false;
    void Start()
    {
        Settings.SetInitAParty(this);
        send.onClick.AddListener(TaskOnClick);
    }

    private void Update()
    {
        if (initReceived)
        {
            waitingRoomPanel.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            TaskOnClick();
        }
    }

    public void TaskOnClick()
    {
        nameErrorMsg.text = "";
        nameErrorMsg.CrossFadeAlpha(1.0f, 0.0f, false);

        if (user.text == "")
        {
            nameErrorMsg.text = "Veuillez entrer un nom";
            nameErrorMsg.CrossFadeAlpha(0.0f, 2.0f, false);
        }
        else if (user.text.Length > 13)
        {
            nameErrorMsg.text = "13 caractères au maximum";
            nameErrorMsg.CrossFadeAlpha(0.0f, 2.0f, false);
        }
        else if (user.text.All(c => Char.IsLetterOrDigit(c)))
        {
            nameErrorMsg.text = "";
            try
            {
                Settings.GetServerConnection().Connect();
                Settings.GetPlayersManager().createLocalPlayer(user.text);
                Settings.GetServerConnection()
                .SendRequest("NEWGAME " + nbPlayer.value + " " + user.text);
            }
            catch (SocketException SE)
            {
                nameErrorMsg.text = "La connexion au serveur a échoué";
                nameErrorMsg.CrossFadeAlpha(0.0f, 2.0f, false);
            }
        }
        else
        {
            nameErrorMsg.text = "Chiffres et lettres uniquement";
            nameErrorMsg.CrossFadeAlpha(0.0f, 2.0f, false);
        }
    }

    public void InitReceived()
    {
        initReceived = true;
    }

}
