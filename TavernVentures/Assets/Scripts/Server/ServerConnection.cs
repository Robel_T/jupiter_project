using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

[CreateAssetMenu(menuName = "Server/Server connection")]
public sealed class ServerConnection : ScriptableObject
{
    private TcpClient serverSocket;
    private NetworkStream stream = null;
    private Thread thread;

    public void Connect()
    {
        serverSocket = new TcpClient();
        serverSocket.Connect("10.192.72.14", 10000); // Serveur : 10.192.72.14
        stream = serverSocket.GetStream();
        thread = new Thread(Listen);
        thread.Start();
    }

    public void Disconnect()
    {
        thread.Abort();
        stream.Close();
        serverSocket.Close();
    }

    // use to send 
    public void SendRequest(string message) 
    {
        try
        {
            StreamWriter writer = new StreamWriter(stream);
            writer.WriteLine(message);
            writer.Flush();
        }
        catch (ArgumentNullException e)
        {
            Console.WriteLine("ArgumentNullException: {0}", e);
        }
        catch (SocketException e)
        {
            Console.WriteLine("SocketException: {0}", e);
        }
    }

    // use to listen actively
    private void Listen()
    {
        StreamReader reader = new StreamReader(stream);
        string text;
        int round = 1;

        while (serverSocket.Connected && (text = reader.ReadLine()) != null)
        {
            string[] orders = text.Split(' ');
            switch (orders[0])
            {
                case "YOURID":
                    Settings.GetPlayersManager().GetCurrentPlayer().setId(Int32.Parse(orders[1]));
                    Settings.ShowWaitingRoom();
                    break;
                case "ROOM":
                    Settings.GetWaitingRoomManager().ManageRoom(orders[1], orders[2]);
                    break;
                case "NOTAVAILABLE":
                    Settings.GetJoinAParty().WrongCode();
                    stream.Close();
                    serverSocket.Close();
                    break;
                case "INIT":
                    Thread.Sleep(4000);
                    Settings.GetWaitingRoomManager().InitGame(orders[1]);
                    break;
                case "HAND":
                    Settings.GetGameManager().MakeHand(orders[1]);
                    break;
                case "BETED":
                    Settings.GetGameManager().ReceivedPlayersBets(orders[1]);
                    break;
                case "TOKEN":
                    Settings.GetGameManager().AllowUserToPLay();
                    break;
                case "STATUS":
                    Settings.GetGameManager().ReceivedPlay(orders[1], orders[2], orders[3]);
                    break;
                case "TURNWINNER":
                    Thread.Sleep(3000);
                    Settings.GetGameManager().ReceivedTurnwinner(Int32.Parse(orders[1]));
                    Thread.Sleep(3000);
                    break;
                case "POINTS":
                    Settings.GetGameManager().ReceivedPoints(orders[1]);
                    if (round++ >= 10)
                    {
                        Settings.GetGameManager().EndGame();
                        stream.Close();
                        serverSocket.Close();
                    }
                    Thread.Sleep(3000);
                    break;
                case "EXCLUDE":
                    Settings.GetGameManager().ReceivedExcluded(Int32.Parse(orders[1]));
                    break;
                case "ABORT":
                    Settings.GetGameManager().EndGame();
                    stream.Close();
                    serverSocket.Close();
                    break;
                case "TIMEOUT":
                    //(); //Je ne pense pas que ce sera utile
                    break;
            }
        }
        //serverSocket.Close();
        Console.Read();
    }
}
