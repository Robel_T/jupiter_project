﻿using System;
using System.Collections;
using System.Linq;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.UI;

public class JoinAParty : MonoBehaviour
{
    public Text nameErrorMsg;
    public Text codeErrorMsg;
    public Button send;
    public Text user;
    public Text room;
    public GameObject waitingRoom;
    public bool initReceived = false;
    public bool isWrong = false;

    void Start()
    {
        Settings.SetJoinAParty(this);
        send.onClick.AddListener(TaskOnClick);
    }

    private void Update()
    {
        if (isWrong)
        {
            isWrong = false;
            codeErrorMsg.text = "Code invalide";
            codeErrorMsg.CrossFadeAlpha(0.0f, 2.0f, false);
        }

        if (initReceived)
        {
            initReceived = false;
            codeErrorMsg.text = "";
            waitingRoom.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            TaskOnClick();
        }
    }

    void TaskOnClick()
    {
        nameErrorMsg.text = "";
        nameErrorMsg.CrossFadeAlpha(1.0f, 0.0f, false);
        codeErrorMsg.text = "";
        codeErrorMsg.CrossFadeAlpha(1.0f, 0.0f, false);

        if (user.text == "")
        {
            nameErrorMsg.text = "Veuillez entrer un nom";
            nameErrorMsg.CrossFadeAlpha(0.0f, 2.0f, false);
        }
        else if (user.text.Length > 13)
        {
            nameErrorMsg.text = "13 caractères au maximum";
            nameErrorMsg.CrossFadeAlpha(0.0f, 2.0f, false);
        }
        else if (user.text.All(c => Char.IsLetterOrDigit(c)))
        {
            nameErrorMsg.text = "";
            try
            {
                Settings.GetServerConnection().Connect();
                Settings.GetPlayersManager().createLocalPlayer(user.GetComponent<Text>().text);
                Settings.GetServerConnection()
                .SendRequest("JOINGAME " + room.GetComponent<Text>().text.ToUpper()
                + " " + user.GetComponent<Text>().text);
            }
            catch (SocketException SE)
            {
                nameErrorMsg.text = "La connexion au serveur a échoué";
                nameErrorMsg.CrossFadeAlpha(0.0f, 2.0f, false);
            }
        }
        else
        {
            nameErrorMsg.text = "Chiffres et lettres uniquement";
            nameErrorMsg.CrossFadeAlpha(0.0f, 2.0f, false);
        }
    }

    public void WrongCode()
    {
        isWrong = true;
    }

    public void InitReceived()
    {
        initReceived = true;
    }
}