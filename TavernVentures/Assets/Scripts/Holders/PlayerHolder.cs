﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Holders/Player holder")]
public sealed class PlayerHolder : MonoBehaviour
{
    public int id;
    public string name;
    public Sprite icon;
    public int points;
    public int currentBets;
    public int maxBets;
    public PlayerStatsUI statsUI;

    public bool canUseCards;
    public bool isExluded = false;

    [System.NonSerialized]
    public List<CardInstance> handCards = new List<CardInstance>();
    [System.NonSerialized]
    public List<GameObject> betChoices = new List<GameObject>();

    public PlayerHolder(string name, Sprite icon, int id)
    {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.points = this.currentBets = this.maxBets = 0;
        this.statsUI = null;
    }

    public void LoadPlayerOnStatsUI()
    {
        if (statsUI != null)
        {
            statsUI.player = this;
            statsUI.UpdateAll();
        }
    }

    public void SetMaxBets(int bet)
    {
        maxBets = bet;
    }

    public void WinTurn()
    {
        currentBets++;
        if (statsUI != null)
        {
            statsUI.UpdatePlayerCurrentBets();
        }
    }

    public void SetPoints(int p)
    {
        points = p;
        currentBets = 0;
        maxBets = 0;

        if (statsUI != null)
        {
            statsUI.DisplayLeaderIcon(false);
            statsUI.UpdatePlayerCurrentBets();
            statsUI.UpdatePlayerPoints();
        }
    }

    public void setId(int id)
    {
        this.id = id;
        this.icon = Settings.GetResourcesManager().GetIcon(id);
    }

    public bool hasColor(CardType color)
    {
        foreach (CardInstance inst in handCards)
        {
            if (inst.cardDisplay.card.type == color)
            {
                return true;
            }
        }
        return false;
    }
}
