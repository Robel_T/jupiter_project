# Jupiter Project
https://robel_t.gitlab.io/jupiter_project/

## Contexte

Le jeu du Skull King est un jeu de cartes dont le but est
d’obtenir le plus de points possibles en pariant 
judicieusement sur ses propres cartes selon si elles 
seront plus fortes que celles des autres joueurs ou non.

Le jeu est jouable de 2 à 6 joueurs en ligne.
 
Pour des raisons juridiques, le nom et l'apparence des cartes ainsi que
le nom du jeu diffèrent du jeu original.

Faire de ce jeu de cartes une version numérique apporte de nombreux avantages :
 * Toutes les opérations chronophages tel que le mélange et la distribution des cartes,
  le décompte des points et le classement des joueurs se font de manière automatique, 
  instantanée et sans erreur.
 * Le jeu est accessible partout, tout le temps et sans avoir à réunir les joueurs en un même lieu.
 * Le respect des règles (comme par exemple l'obligation de poser une carte spécifique) est assuré,
  empêchant toute tricherie ou erreur d'inattention.
 * Les cartes étant numériques, impossible de les perdre. 
 

## Règles

Le jeu se déroule en 10 manches, chaque joueur a autant de cartes 
en main que le numéro de la manche en cours.
Une fois sa ou ses cartes en main, les joueurs vont énoncer le nombre de plis qu’ils pensent faire
durant la manche. Puis chacun leur tour, les joueurs vont jouer une carte.
Comme au chibre, la première couleur jouée doit obligatoirement être suivie, c'est-à-dire que
les joueurs suivants sont obligés de jouer la couleur annoncée s'il la possède.
S'ils ne l'ont pas, ils peuvent soit jouer une autre couleur, soit une carte spéciale.
La hiérarchie des cartes est la suivante : 
 * les cartes spéciales battent les couleurs, sauf les drapeaux qui eux sont tout en bas de
 la hiérarchie.
 * La couleur noir bat toutes les autres couleurs. Mais contrairement au chibre, 
 nous ne pouvons pas jouer cette couleur si la pli est d'une autre couleur et que nous
 possédons une carte de ladite couleur.
 * La hiérarchie des cartes de couleurs va du plus petit au plus grand.
 * Dans les cartes spéciales le skull-king bat les pirates, les pirates battent les sirènes,
 les sirènes battent le Skull King. Si les trois sont présentes sur le terrain, la sirène 
 est gagnante.
 * Si plusieurs cartes sont aux mêmes niveau dans la hiérarchie, la première posée mène la partie.
Le système de points marche comme suit :
 * Si un joueur a gagné tous ses paris, il gagne 20 points multipliés par le nombre de plis remportés.
 * À l'inverse, si un joueur n'a pas gagné tous ses paris, il perd 10 points par pli 
 en trop ou par pli en manque.
 * Si un joueur gagne avec le Skull King, il gagne 30 points par pirate posé durant la pli.
 * Si un joueur gagne avec la sirène et que le Skull King est dans la pli, il gagne 50 points.
 * Les deux points ci-dessus ne s'appliquent que si le joueur a gagné tous ses paris.
 * Si un joueur parie 0, il gagne 10 points multipliés par le nombre de tours.
 Par contre, s'il fait au moins une pli, il perdra le même montant à la place.
 
 Le jeu comporte 1 Skull King, 6 pirates (dont un interchangeable avec un drapeau blanc),
 2 sirènes, 5 drapeaux blancs et des cartes de couleurs (rouge, bleu, jaune et noir) 
 allant de 1 à 13, soit 66 cartes en tout.
 
## Spécifications
 
 La partie client de notre projet sera créée sur Unity en C#.
 La partie serveur de l'application sera codée en Java.
 
 Au lancement du jeu, après qu'il ait choisi son nom et son portrait,
  le joueur arrive sur un menu d'accueil lui offrant 3 options :
 * Créer une partie
 * Rejoindre une partie
 * Consulter les règles et cartes
 
### Créer une partie

Le joueur choisit le nombre maximal de joueurs qu'il souhaite pour cette partie. 
Puis l'application lancera au serveur une demande.

Le serveur crée un nouveau thread correspondant à cette partie-ci.

Ce nouveau thread annonce à l'utilisateur son id.

Ayant reçu l'id, le joueur arrive dans une "salle d'attente" où il reçoit régulièrement des informations
sur les joueurs qui le rejoignent.

### Rejoindre une partie

Pour qu'un joueur rejoigne une partie, il faut au préalable qu'un autre joueur ait créé une partie
et qu'il lui passe l'id de la partie en cours.

Ensuite, le joueur rentre cet id pour rejoindre la même "salle d'attente" que 
le joueur ayant créé la partie.

Si l'id de la partie n'est pas valide, si le nombre maximal de joueurs est atteint ou si 
la partie a déjà commencé, le serveur indique que la partie voulue n'est pas disponible.

### Consulter les règles et cartes

Un joueur a aussi la possibilité de consulter les règles et les cartes via la page d'accueil. 

Cette action ne sollicite pas le serveur, elle est simplement gérée par l'application
 côté client.


### Partie

Lorsque le nombre de joueurs dans la "salle d'attente" arrive à 2, un bouton "commencer"
 est alors disponible pour le créateur de la partie. Quand il clique dessus, la partie se lance.

Le déroulement de la partie se fait conformément aux spécifications citées dans la partie Règles
 de ce document.

Si une erreur de connexion est perçue par le serveur, le joueur en question est exclu de la 
partie. Son nom reste toutefois sur l'écran des joueurs, mais son tour sera sauté et il 
sera indiqué dans le classement final qu'il aura abandonné la partie.

La phase de choix du nombre de plis et celle du choix de la carte à jouer ont chacune
un décompte de 15 secondes. Si aucun choix n'a été effectué, le nombre de plis ou
la carte à jouer sont choisis aléatoirement.
 
A la fin de la 10ème manche, ou s'il ne reste plus qu'un joueur, la connexion avec
le serveur est interrompue.

Toutes les actions effectuées sont conservées dans les logs. Certaines pouvant être
affichées durant la partie (connexion, déconnexion, vainqueur du tour, nombre de points gagnés, joueur en tête, etc).
 
## Améliorations potentielles

Mettre le serveur en ligne (de base, le serveur ne s'exécute que localement).

Recherche de parties en ligne contre des joueurs du monde entier.

Pouvoir jouer contre des IA.

Permettre une reconnexion à la partie en cas de déconnexion.

Ajout d'un tutoriel guidé pour apprendre les règles et les cartes.

Ajout de modes de jeux.

Ajout d'animations.

Ajout d'apparences personnalisables (portraits, dos de cartes, fond d'écran).

Ajout d'effets sur les cartes.
 
Cross-platforms.

## MVP
Le joueur peut entrer son pseudo, créer une partie, rejoindre une partie ou consulter les règles et cartes. Graphisme minimal, aucun sons.

Seules les cartes de couleurs sont disponibles. 4 couleurs : 3 basiques (rouge, jaune, bleu) et une dominante (noir).
Chaque couleur ayant 13 cartes numérotées de 1 à 13. Illustrations basiques.

### Créer une partie
Le joueur se retrouve dans une salle d'attente avec un code. 
Une fois les 2 joueurs présents, le joueur ayant créé la partie peut la lancer.

### Rejoindre une partie 
Le joueur peut rejoindre une salle d'attente spécifique en entrant le code d'une partie.
Il est impossible de rejoindre une partie si le nombre de joueurs maximal est atteint ou si la partie est déjà lancée.

### Consulter les règles et cartes
Les règles du jeu ainsi que les catégories de cartes peuvent être consultées.

### Déroulement d'une partie
Une partie se joue en 10 manches.
Une manche est composée d'autant de tours que de cartes reçues
(manche 1 = 1 carte par joueur = 1 tour, manche 2 = 2 cartes par joueurs = 2 tours, ..., manche 10 = 10 cartes par joueur = 10 tours).

Au début de chaque manche, les cartes sont mélangées puis les 2 joueurs reçoivent chacun autant de cartes que le numéro de la manche actuelle.
*Les joueurs ont ensuite 15 secondes pour choisir un chiffre compris entre 0 et le numéro de la manche actuelle correspondant.*
Le chiffre qu'ils auront choisi correspond au nombre de tours qu'ils pensent gagner grâce à la carte qu'ils auront posé.

*À chaque tour, chaque joueur à 15 secondes pour poser une carte.* Le joueur ayant posé la carte la plus forte remporte le tour.

Règles s'appliquant sur les cartes :
 * La couleur noir bat les autres couleurs.
 * La 1ère couleur posée doit être suivie par l'autre joueur, s'il la possède. Exemple : le joueur 1 pose une carte bleue. Si le joueur 2 possède au moins une carte bleue en main, il est obligé de la jouer.
 * La 1ère couleur basique posée domine les autres couleurs basiques. Exemple : le joueur 1 pose un 10 bleu. Pour gagner, le joueur 2 doit soit poser une carte bleue supérieure (11, 12 ou 13), soit une carte noire.
																	 Le joueur 2 perd forcément s'il joue une carte rouge ou jaune peu importe sa valeur, dans ce cas-là.

Chaque tour est forcément remporté par l'un des 2 joueurs.

À la fin de chaque manche, les joueurs gagnent ou perdent des points selon si leur pari s'est révélé correct ou non,
à savoir s'ils ont gagné le bon nombre de tours qu'ils avaient prédit au début de la manche.

La partie se termine lorsque 10 manches ont été jouées. Le joueur ayant le plus de points remporte la partie.

### Éléments affichés durant une partie
* Le nom de chaque joueur avec leur score total actuel et l'état actuel de leur pari.
* Les cartes de la main du joueur.
* Les cartes posées sur la table durant le tour actuel.
* Décompte du temps restant pour la phase de pari et celle du choix de la carte à jouer.
* Message d'information selon la phase de jeu (début de manche (pari) : "Faites vos jeux", choix de la carte à jouer : "À votre tour", "Au tour de <Joueur 2>",
											   fin de tour : "Vous remportez ce tour", "<Joueur 2> remporte ce tour", fin de manche : "Manche terminée. Attribution des points",
											   fin de partie : "Vous remportez la partie", "<Joueur 2> remporte la partie").