# Mes opinions

Je penses que nous devrons créer les points suivants :

  * Un serveur gérant la distribution des cartes et les joueurs.
  * Un client qui pourra voir les cartes qu'il possède et prendre les décisions.
  * Une base de données qui stoque les cartes et les joueurs.