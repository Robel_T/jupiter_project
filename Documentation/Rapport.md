# Versions utilisées
Pour réaliser ce projet, nous avons utilisé :
 * Pour le serveur, l'éditeur Intellij 2019.1 avec la jdk1.8.0_141.
 * Pour le client, Unity 2019.2.9f1.

# Explication du projet
## Le jeu
Notre projet consiste à implémenter le jeu du skull king dans une version  informatisée.

Pour simplifier la charge de travail, nous n'avons pas implémenté les cartes "habillées"
et avons remplacé les 3 cartes manquantes par des cartes portes (de plus petites importances).

## Le serveur
Le serveur est fait en java. Il est chargé de :
 1. Disperser les joueurs, soit en créant une nouvelle salle de jeu, soit en laissant le choix à un joueur 
 de rejoindre une salle de jeu, en rentrant l'id de cette salle.
 2. Gérer le tour des joueurs, leurs différentes phases, les éventuelles déconnexions et les points de chaque joueur, puis de l'envoyer 
 à chaque joueur.
 3. De créer des logs pour avoir une trace de la partie. Cela peut être utile en cas de crash d'une partie
 (savoir ce qui s'est passé) ou pour faire des statistiques.
 
## Le client
Le client est fait en partie en C# avec le moteur graphique de Unity.

Sa fonctionnalité principale est de servir d'interface entre le serveur et l'utilisateur, de sorte qu'à ce 
que ce dernier n'ait pas à jouer en ligne de commande en connaissant par coeur le protocole de communication.

Il sert aussi à mettre des timers pour que les autres joueurs n'aient pas à attendre sur un joueur lambda 
qui serait en train de faire autre chose. Une fois un timer fini, ce dernier jouera à la place du joueur absent.

## Le protocole de communication
Le protocole utilisé est celui spécifié ci-dessous:
### NEWGAME 
Le message qu’envoie le joueur qui souhaite commencer une nouvelle partie. Ce message contiendra comme arguments le nom du joueur et le nombre de participants maximal souhaité.
### ROOM
Ce message est envoyé par le serveur aux joueurs pour indiquer l’id de la room et les joueurs présents pour le moment (se réactualise à chaque nouveau joueur). Ce message contiendra comme arguments l’id de la room et un tableau contenant le nom des joueurs présents, l’id que le serveur leur a donné.
### YOURID 
Ce message indique au joueur quel id il aura dans la partie lorsqu’il s’inscrira sur le serveur.
### JOINGAME
Ce message est envoyé par un autre joueur pour pouvoir rejoindre une partie créée par un ami. Ce message contiendra comme arguments le nom du joueur et l’id de la partie.
### NOTAVAILABLE
Ce message est envoyé par le serveur en réponse d’un JOINGAME lorsque l’id demandé ne correspond à aucune partie, la partie correspondant à cet id a déjà le nombre de joueurs maximal ou lorsque la partie a déjà commencé. Ce message ne contient aucun argument.
### BEGIN 
Ce message est envoyé par le joueur qui a initié la partie quand il estime que la partie peut commencer. Il ne contient aucun argument.
### INIT
Ce message est envoyé par le serveur à tous les joueurs pour leur indiquer que la partie commence. Il comprend comme arguments l’id et le nom de tous les joueurs.
### HAND
Ce message est envoyé par le serveur à un joueur. Cela correspond aux cartes qu’il aura dans sa main lors de la manche en cours. Ce message contiendra comme argument une liste d’id décrivant les cartes que le joueur aura durant ce tour.
### BAIT
Ce message est envoyé par un joueur au serveur indiquant à ce dernier combien il pense faire de plies durant la manche en cours. 
### BAITED
Ce message est envoyé par le serveur à tous les joueurs leurs indiquant ce que les autres joueurs ont parié pour la manche en cours, lorsqu’il a reçu tous les paris. Ce message contient comme argument une liste contentant l’id des joueurs et leur pari respectif. 
### TOKEN 
Ce message est envoyé par le serveur à un joueur, lui indiquant que c’est à son tour de poser une carte. Ce message ne comporte pas d’argument.
### PLAY
Ce message est envoyé par le joueur au serveur quand c’est à son tour de poser une carte. Il contient comme unique argument l’id de la carte en question.
### STATUS
Ce message est envoyé par le serveur à tous les joueurs après qu’un joueur ait posé une carte. Ce message comporte comme arguments l’id de la carte qui vient d’être jouée, l’id de la carte qui mène actuellement et la couleur de la plie.
### TURNWINNER
Ce message est envoyé par le serveur à tous les joueurs pour leur indiquer qui a gagné le tour courant. Il prend comme argument l’id du joueur qui remporte le tour uniquement.
### POINTS
Ce message est envoyé par le serveur à tous les joueurs à la fin d’une manche. Il indique à ces derniers quel est le score de chaque joueur à la fin de ladite manche. Il prend comme arguments l’id des joueurs et leur nombre de points respectifs.
### EXCLUDE
Ce message est envoyé par le serveur si le fait de lire le BufferedReader d’un client lui génère une erreur. Nous partons du principe que ce client s’est déconnecté. Il sera donc ignoré pendant le reste de la partie. Ce message comprend en argument le numéro du client ainsi exclu.
### ABORT
Ce message est envoyé par le serveur au seul client qui ne s’est pas encore déconnecté (s’il en reste un). Lorsqu’il ne reste plus qu’un joueur, la partie est annulée. Ce message est là pour en avertir le client. Il ne comprend aucun argument.
### TIMEOUT
Ce message est envoyé par le serveur à tous les clients connectés lorsqu’il attend des joueurs depuis plus de 5 minutes. Si le nombre de joueurs est de 1, il mettra fin à la partie, sinon il lance la partie avec les joueurs restants.
N'est pas réellement utilisé dans cette implémentation.

La connexion avec le serveur sera fermée à la fin de la 10ème manche

# Vision du projet
Ce projet fait partie de notre plan d'étude. Il n'est donc pas fait pour être commercialisé, mais dans une 
optique d'apprentissage que ça soit dans l'optimisation de nos compétences techniques ou dans notre capacité
à travailler en groupe et à nous répartir les différentes tâches de manière équitable et juste.

La version finale du projet sera une version jouable à plusieurs (maximum 6).
 
# Maintenabilité
## Serveur
### Partie réception
La classe chargée de recevoir les clients et les dispatcher dans les différentes salles d'attente est la classe ServerManager
### Partie partie (jeu)
La classe chargée d'ordonner le déroulement d'une partie (de l'entrée en salle d'attente jusqu'à la fin de la partie)
est la classe GameServer
### Partie joueur
La classe contenant les données des joueurs est la classe Player
### Partie règles
La classe s'occupant de définir les règles du jeu est la classe Rule
### Partie points
La classe chargée de comptabiliser les points d'un joueur est la classe Point
### Partie cartes
Les classes s'occupant des cartes sont toutes les classes héritant de la classe abstraite Card.

La classe chargée de définir les cartes utilisées dans la partie et de leur distribution est la classe CardPackage
## Client
### Partie serveur
La classe chargée d'initialiser la connexion avec le serveur, de l'écouter, de lui envoyer des messages et 
de fermer la connexion est la classe ServerConnexion.

La classe chargée d'initier la partie est la classe InitAParty.

La classe chargée de rejoindre une partie est la classe JoinAParty.
### Partie joueur
La classe s'occupant des données des joueurs est la classe PlayerHolder.

La classe chargée de créer les joueurs de savoir lequel d'entre eux est le joueur courant, de changer leur id et 
d'accéder aux joueurs est la classe PlayersManager.
### Partie icônes
La classe chargée d'initialiser les icônes des cartes et des joueurs est la classe ResourcesManager.

Toutes les icônes sont aussi définies manuellement dans les différentes scènes de notre projet Unity.
### Partie cartes
Les classes chargées de s'occuper des cartes (définition, comportement, etc...) sont les classes Card, CardInstance,
CardDisplay, MouseHoldingCard, CardDown et CardsDownAreaLogic.

On trouve les images correspondantes dans le dossier Artwork et leurs données dans le dossier Data/Cards.
### Partie interaction avec le joueur
La classe s'occupant d'initialiser une phase de pari est BetPhase.

La classe s'occupant d'initialiser une phase de jeu est PlayCardPhase.
### Partie jeu
La classe s'occupant de réagir aux différentes instructions du serveur est la classe GameManager.

### Partie principale
La classe reliant tous les managers (et autres) est la classe Settings. Cette dernière est automatiquement lancée 
lorsque que le projet démarre.

### Autres remarques
Toutes les classes héritant de MonoBehaviour sont lancées lorsque la scène (ou autre GameObject) à laquelle elles
sont liées est active. C'est à ce moment précis qu'elles prennent leurs valeurs par défaut (soit fait la correspondance
entre les éléments de la scène et les variables GameObject (ou sous-classe) qu'elles manipulent). 

Si vous souhaitez modifier une des classes héritant de MonoBehaviour (par exemple en rajoutant des nouveaux messages
du serveur), sachez que vous ne pourrez pas changer directement les variables GameObject, car seul le thread principal
de la classe le peut. Pour effectuer cette action, vous êtes obligés de passer par des variables intermédiaires qui seront 
modifiées à la réception du message du serveur, pour ensuite modifier les variables GameObject dans la méthode Update 
de ladite classe.

### Build le client avec Unity
Pour créer un exécutable de notre client, procéder comme suit :
 1. Dans Unity, cliquez sur File, puis Build Settings. La page suivante devrait s'afficher.
 
 ![BuildTheApplication.PNG](https://gitlab.com/Robel_T/jupiter_project/blob/44-faire-la-documentation/Documentation/ScreenShot/BuildTheApplication.PNG)
 
 2. Après avoir sélectionné le chemin dans lequel vous souhaitez créer votre exécutable, appuyer sur le bouton Build
 3. Après quelques dizaines de secondes, les fichiers suivants devraient avoir été créés.
 
 ![BuildCreateFile.PNG](https://gitlab.com/Robel_T/jupiter_project/blob/44-faire-la-documentation/Documentation/ScreenShot/BuildCreatedFile.PNG)
 
### Lancer les tests dans Unity
Pour lancer les tests dans Unity, vous devez ouvrir l'onglet test runner.

Pour cela cliquer sur Window, allez dans l'onglet General, puis cliquez sur Test Runner.

![GetTheTestRunnerWindow.PNG](https://gitlab.com/Robel_T/jupiter_project/blob/44-faire-la-documentation/Documentation/ScreenShot/GetTheTestRunnerWindow.PNG)

Ensuite, vous n'avez plus qu'à faire runner les tests (via le bouton runall ou autre).

# Bugs connus
Il se peut que la carte reste bloquée dans la main. Si ce bug se produit, cette dernière restera dans la main
tant que vous n'avez pas cliqué sur une autre carte. Ceci est un bug d'affichage. La carte est en fait retournée
dans la main mais ne s'affiche pas. La carte ainsi "perdue" sera jouée automatiquement quand elle le devra (à la
fin d'un timer s'il est possible de la jouer). 
 
Après que le dernier joueur ait joué sa carte lors d'un tour, le message "Au tour de <XXX>" s'affichera 
brièvement avant d'afficher le nom du joueur remportant la manche. Ce bug d'affichage peut porter à 
confusion.

Parfois, le client plante lorsque le serveur lui indique une fin de partie (peut-être parce qu'il ne reçoit 
pas le message ABORT avant la déconnexion).

Même si un client ferme la connexion avec le serveur, il ne semble pas que la connexion soit totalement
interrompue (sauf si le client ferme le programme).
 
Dans de très rare cas, le programme plante pour une raison inconnue lorsque l'on arrive dans la salle d'attente
car il semblerait qu'il n'arrive pas à charger l'icône du joueur (icône qui apparaît pourtant bien dans
la salle d'attente ?)

# Crédits
 * UI : Yoann Rohrbasser
 * DevOps : Robel Teklehaimanot
 * FrontEnd : Loris Crüll
 * BackEnd : Julien Rod
 * Design des cartes : Elodie Lagier
 * Image de fond d'écran trouvée sur : https://wallpaperaccess.com/tavern
