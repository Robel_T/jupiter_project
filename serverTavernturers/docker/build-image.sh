#!/bin/bash

# Ask maven to build the executable jar file from the source files
mvn clean install --file ../pom.xml

# Copy the executable jar file in the current directory
cp ../target/jupiter.project-1.0-SNAPSHOT-launcher.jar .



#Kill and delete all container
docker kill $(docker ps -a -q)
docker rm $(docker ps -a -q)

#delete image server-pdg
docker rmi server-pdg

# Build the Docker image locally
docker build --tag server-pdg .

