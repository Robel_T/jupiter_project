//Inspiré de https://github.com/SoftEng-HEIGVD/Teaching-HEIGVD-RES-2019/blob/master/examples/07-TcpServers/TcpServers/src/main/java/ch/heigvd/res/examples/MultiThreadedServer.java
package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
//

public class ServerManager implements Runnable {
    private static Map<String, GameServer> gameServers = new HashMap<String, GameServer>();
    private static final char[] characters = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
            'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    private static final int nbServersPossible = (int) Math.pow(characters.length, 5);
    private final Logger LOG = Logger.getLogger(ServerManager.class.getName());
    private final int port;
    private BufferedReader reader = null;
    private PrintWriter writer = null;

    private ServerSocket serverSocket;

    public ServerManager(int port){
        this.port = port;
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            LOG.log(Level.SEVERE, null, e);
            return;
        }
    }

    public String generateNewGameServer(int maxPlayers, boolean test){
        String gameServerId = "";
        Random rand = new Random(System.currentTimeMillis());
        do {
            for (int i = 0; i < 5; ++i) {
                gameServerId += characters[rand.nextInt(characters.length)];
            }
        }while(gameServers.containsKey(gameServerId));
        gameServers.put(gameServerId, new GameServer(gameServerId, maxPlayers, test));
        return gameServerId;
    }

    public static boolean isNumber(String s){
        try {
            Integer.parseInt(s);
        }catch(NumberFormatException e){
            return false;
        }
        return true;
    }

    public static void deleteGameServer(String id){
        gameServers.remove(id);
    }

    public void run() {
        LOG.log(Level.INFO, "ServerManager has started.");
        while(true){
            try {
                Socket clientSocket = serverSocket.accept();
                LOG.info("A new client has arrived.");
                NonBlockingThread nbt = new NonBlockingThread(clientSocket);
                nbt.Start();
                new Timer(nbt).Start();
            } catch (IOException e) {
                Logger.getLogger("Problem accepting client : " + e);
            }
        }
    }

    private class NonBlockingThread extends Thread{
        private Socket clientSocket;
        public NonBlockingThread(Socket clientSocket){
            this.clientSocket = clientSocket;
        }

        public void Start(){
            try {
                reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                writer = new PrintWriter(clientSocket.getOutputStream());
                String[] requete = reader.readLine().split(" ");
                if(requete.length == 3 && "NEWGAME".equals(requete[0]) && isNumber(requete[1])
                        && Integer.parseInt(requete[1]) >= 2 && Integer.parseInt(requete[1]) <= 6
                        && gameServers.size() <= nbServersPossible){
                    //Commence le jeu
                    String gameServerId = generateNewGameServer(Integer.parseInt(requete[1]), false);
                    GameServer gs = gameServers.get(gameServerId);
                    gs.start();
                    gs.addPlayer(writer, reader, requete[2]);
                }else if(requete.length == 3 && "JOINGAME".equals(requete[0])
                        && gameServers.size() <= nbServersPossible){
                    //On fait rejoindre le joueur
                    if(gameServers.containsKey(requete[1]) && gameServers.get(requete[1]).isAccessible()){
                        gameServers.get(requete[1]).addPlayer(writer, reader, requete[2]);
                    }else{
                        writer.println("NOTAVAILABLE");
                        writer.flush();
                        reader.close();
                        writer.close();
                    }
                }else{
                    reader.close();
                    writer.close();
                }
            } catch (IOException e) {
                writer.println("Invalid command received");
                writer.flush();
                Logger.getLogger(ServerManager.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    private class Timer extends Thread{
        private NonBlockingThread nbt;
        public Timer(NonBlockingThread nbt){
            this.nbt = nbt;
        }

        public void Start(){
            try {
                Thread.sleep(5000);
                nbt.interrupt();
            } catch (InterruptedException e) {
                e.printStackTrace( );
            }
        }
    }
}
