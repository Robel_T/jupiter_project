package server;

public class Pair<T, S> {
    private T key;
    private S value;

    public Pair(T first, S second) {
        this.key = first;
        this.value = second;
    }

    public String toString() {
        return key + "=" + value;
    }

    /**
     * Gets the key for this pair.
     * @return key for this pair
     */
    public T getKey() { return key; }

    /**
     * Gets the value for this pair.
     * @return value for this pair
     */
    public S getValue() { return value; }

    public int hashCode() {
        // name's hashCode is multiplied by an arbitrary prime number (13)
        // in order to make sure there is a difference in the hashCode between
        // these two parameters:
        //  name: a  value: aa
        //  name: aa value: a
        return key.hashCode() * 13 + (value == null ? 0 : value.hashCode());
    }


    public boolean equals(Object o) {
        if (this == o) return true;
        if (o instanceof Pair) {
            Pair pair = (Pair) o;
            if (key != null ? !key.equals(pair.key) : pair.key != null) return false;
            if (value != null ? !value.equals(pair.value) : pair.value != null) return false;
            return true;
        }
        return false;
    }
}