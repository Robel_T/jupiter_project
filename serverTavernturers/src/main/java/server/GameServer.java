package server;

import card.Card;
import card.Color;
import cardPackage.CardPackage;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import player.Player;
import points.Point;
import rules.Rule;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import static java.lang.System.currentTimeMillis;
//

public class GameServer extends Thread {
    private final Logger LOG = Logger.getLogger(GameServer.class.getName());
    PrintWriter logWriter;
    private final String id;
    private final int maxPlayers;
    private int nbPlayers;
    private final LinkedList<Player> players;
    protected Point points;
    protected int[] playerBets, doneBets;
    protected Pair<Card, Player> actualLeader;
    protected Color stackColor;
    protected LinkedList<Card> stack;
    protected GameState state;


    public GameServer(String id, int maxPlayers, boolean test) {
        this.id = id;
        this.nbPlayers = 0;
        this.maxPlayers = maxPlayers;
        this.players = new LinkedList<>();
        this.stack = new LinkedList<>();
        this.state = GameState.WAITINGROOM;
        if (!test) {
            try {
                File file = new File("logs/" + currentTimeMillis( ) + "-" + id + ".txt");
                //file.mkdirs();
                file.createNewFile( );
                logWriter = new PrintWriter(file, "UTF-8");
            } catch (IOException e) {
                e.printStackTrace( );
            }
        }
    }

    public String getIdentification(){
        return id;
    }

    public int size(){ return nbPlayers;}

    public void run() {
        //Attente
        LOG.info(id + " Waiting for players.");
        if(logWriter != null) logWriter.write(id + " Waiting for players.\r\n");
        createTimer(300000);
        waitForPlayers();
        if(players.size() < 2) {
            sendToAllPlayer("ABORT");
            closeConnexion();
            return;
        }
        //Inique au joueur que le jeu commence
        LOG.info(id + " The game will begin.");
        if(logWriter != null) logWriter.write(id + " The game will begin.\r\n");
        playerBets = new int[nbPlayers];
        doneBets = new int[nbPlayers];
        points = new Point(nbPlayers);
        sendToAllPlayer("INIT " + getPlayers());
        LOG.info(id + " Players :" + getPlayers());
        if(logWriter != null) logWriter.write(id + " Players :" + getPlayers() + "\r\n");
        //Commence la partie
        playGame();
        //Fini la partie
        LOG.info(id + " The game is over.");
        if(logWriter != null) logWriter.write(id + " The game is over.\r\n");
        shutDown();
    }

    protected Player addPlayer(PrintWriter playerWriter, BufferedReader playerReader, String playerName){
        Player p = new Player(this.nbPlayers, playerName, playerReader, playerWriter);
        players.add(p);
        playerWriter.println("YOURID " + this.nbPlayers++);
        sendToAllPlayer("ROOM " + id + " " + getPlayers());
        return p;
    }

    public boolean isAccessible(){
        return (players.size() < maxPlayers) && state == GameState.WAITINGROOM;
    }

    public GameState getGameState(){
        return this.state;
    }

    public String getPlayers(){
        JSONArray playersList = new JSONArray();
        for(int i = 0; i < players.size(); ++i){
            if(!players.get(i).isExclude()) {
                JSONObject player = new JSONObject( );
                player.put("id", i);
                player.put("name", players.get(i).getName( ));
                playersList.add(player);
            }
        }
        JSONObject players = new JSONObject();
        players.put("players", playersList);
        return players.toJSONString();
    }

    protected void sendToAllPlayer(String message){
        for(Player player : players){
            if(!player.isExclude()) {
                player.getWriter( ).println(message);
                player.getWriter( ).flush( );
            }
        }
        LOG.info(id + " " + message);
    }

    public void waitForPlayers(){
        while(isAccessible()){
            try {
                synchronized(players) {
                    if (players.size( ) > 1) { //Si joueur 1 lance la game
                        BufferedReader firstPlayerReader = players.get(0).getReader( );
                        if (firstPlayerReader.ready( ) && firstPlayerReader.readLine( ).equals("BEGIN")) {
                            this.state = GameState.INGAME;
                        }
                    }
                }
            } catch (IOException | NullPointerException e) {
                LOG.info(id + " Problem with sleep statement : " + e);
                if(logWriter != null) logWriter.write(id + " Problem with sleep statement : " + e + "\r\n");
            }
        }
    }

    protected void playGame(){
        if(state == GameState.WAITINGROOM) state = GameState.INGAME;
        for(int round = 1; round <= CardPackage.NBFINALROUND; ++round){
            if(state == GameState.ZOMBIE) break; //En cas d'abort ou de time out
            resetRound();
            playRound(round);
        }
    }

    protected void playRound(int round){
        CardPackage deck = new CardPackage();
        distributeCards(deck, round);
        waitForBets();
        sendToAllPlayer("BETED " + getPlayersBets());
        int firstPlayer = round;
        for(int turn = 1; turn <= round; ++turn){
            resetTurn();
            firstPlayer = playTurn(firstPlayer);
            sendToAllPlayer("TURNWINNER " + firstPlayer);
            if(state == GameState.ZOMBIE) break; //En cas d'abort ou de time out
        }
        points.updatePoint(playerBets, doneBets, round);
        sendToAllPlayer("POINTS " + getPlayersPoints());
    }

    protected int playTurn(int firstPlayer){
        for(int i = 0; i < players.size(); ++i){
            if(!players.get((firstPlayer + i) % players.size()).isExclude()) {
                playerTurn(players.get((firstPlayer + i) % players.size()));
            }
            if(state == GameState.ZOMBIE) return -1; //En cas d'abort ou de time out
        }
        doneBets[actualLeader.getValue().getId()] += 1;
        return actualLeader.getValue().getId();
    }

    protected void playerTurn(Player player){
        player.getWriter().println("TOKEN");
        player.getWriter().flush();
        try {
            String[] playerPlay = player.getReader().readLine().split(" ");
            if(playerPlay.length == 2 && "PLAY".equals(playerPlay[0])){
                Card playedCard = Card.recreateCardFromId(playerPlay[1]);
                stack.add(playedCard);
                if(stackColor == Color.COLORLESS) {
                    stackColor = playedCard.getColor();
                }
                Card leader = Rule.setLeadingCard(playedCard, actualLeader.getKey());
                if(playedCard == leader){
                    actualLeader = new Pair<>(playedCard, player);
                }
                sendToAllPlayer("STATUS " + getPlay("newPlay",new Pair<>(playedCard, player)) + " "
                        + getPlay("leader", actualLeader) + " "+ stackColor);
            }
        } catch (SocketException | NullPointerException e){
            excludePlayer(player);
        } catch (IOException e) {
            LOG.info(id + " Problem with the player turn : " + e);
            if(logWriter != null) logWriter.write(id + " Problem with the player turn : " + e + "\r\n");
        }
    }

    public void distributeCards(CardPackage deck, int manche){
        for(Player player : players){
            if(!player.isExclude()) {
                String hand = "HAND " + getPlayerCards(deck.distribution(manche));
                player.getWriter( ).println(hand);
                player.getWriter( ).flush( );
                if(logWriter != null) logWriter.write(id + " " +player.getId() + " -> " + hand + "\r\n");
            }
        }
    }

    public String getPlayerCards(List<Card> cards){
        JSONArray hand = new JSONArray();
        for(Card c : cards){
            JSONObject card = new JSONObject();
            card.put("card", c.getId());
            hand.add(card);
        }
        JSONObject result = new JSONObject();
        result.put("hand", hand);
        return result.toJSONString();
    }

    public String getPlayersBets(){
        JSONArray bet = new JSONArray();
        for(int i = 0; i < players.size(); ++i){
            if(!players.get(i).isExclude()) {
                JSONObject card = new JSONObject( );
                card.put("id", players.get(i).getId( ));
                card.put("bet", playerBets[i]);
                bet.add(card);
            }
        }
        JSONObject result = new JSONObject();
        result.put("bets", bet);
        if(logWriter != null) logWriter.write(id + " " + result.toJSONString() + "\r\n");
        return result.toJSONString();
    }

    protected String getPlayersPoints(){
        JSONArray playerPoints = new JSONArray();
        int[] actualPoint = points.actualPoint();
        for(int i = 0; i < players.size(); ++i){
            if(! players.get(i).isExclude()) {
                JSONObject point = new JSONObject( );
                point.put("id", players.get(i).getId( ));
                point.put("points", actualPoint[i]);
                playerPoints.add(point);
            }
        }
        JSONObject result = new JSONObject();
        result.put("playerPoints", playerPoints);
        if(logWriter != null) logWriter.write(id + " " + result.toJSONString() + "\r\n");
        return result.toJSONString();
    }

    protected String getPlay(String type, Pair<Card, Player> cardPlayer){
        JSONObject pair = new JSONObject();
        pair.put("card", cardPlayer.getKey().getId());
        pair.put("player", cardPlayer.getValue().getId());
        JSONObject result = new JSONObject();
        result.put(type, pair);
        if(logWriter != null) logWriter.write(id + " " + result.toJSONString() + "\r\n");
        return result.toJSONString();
    }

    protected void waitForBets(){
        for(int i = 0; i < players.size(); ++i){
            if(!players.get(i).isExclude()) {
                try {
                    String[] bet = players.get(i).getReader( ).readLine( ).split(" ");
                    if (bet.length == 2 && "BET".equals(bet[0]) && ServerManager.isNumber(bet[1])) {
                        playerBets[i] = Integer.parseInt(bet[1]);
                    }
                } catch (SocketException | NullPointerException e){
                    excludePlayer(players.get(i));
                } catch (IOException e) {
                    LOG.info(id + " Problem in bet statement." + e);
                    if(logWriter != null) logWriter.write(id + " Problem in bet statement." + e + "\r\n");
                }
            }
        }
    }

    protected void resetRound(){
        for(int i = 0; i < players.size(); ++i){
            playerBets[i] = doneBets[i] = 0;
        }
    }

    protected void resetTurn(){
        stack.clear();
        stackColor = Color.COLORLESS;
        actualLeader = new Pair<Card, Player>(null, null);
    }

    protected void closeConnexion(){
        for(Player player : players){
            if(!player.isExclude()) {
               closePlayerConnexion(player);
            }
        }
    }

    protected void closePlayerConnexion(Player player){
        try {
            player.getReader( ).close( );
            player.getWriter( ).close( );
        } catch (IOException e) {
            LOG.info(id + " Problem when closing connexion : " + e);
            if(logWriter != null) logWriter.write(id + " Problem when closing connexion : " + e + "\r\n");
        }
    }

    protected void excludePlayer(Player player){
        player.exclude();
        closePlayerConnexion(player);
        LOG.info(id + " Exclude player : " + player.getId());
        if(logWriter != null) logWriter.write(id + " Exclude player : " + player.getId() + "\r\n");
        sendToAllPlayer("EXCLUDE " + player.getId());
        if(--nbPlayers < 2){
            sendToAllPlayer("ABORT");
            if(logWriter != null) logWriter.write(id + " Abort\r\n");
            shutDown();
        }
    }

    protected void timeOut(){
        LOG.info(id + " Time out.");
        if(logWriter != null) logWriter.write(id + " Time out.\r\n");
        sendToAllPlayer("TIMEOUT");
        state = GameState.INGAME;
    }

    protected void shutDown(){
        state = GameState.ZOMBIE;
        closeConnexion();
        if(logWriter != null) logWriter.close();
        ServerManager.deleteGameServer(this.id);
    }

    protected void createTimer(int time){
        new Timer(time).start();
    }

    public class Timer extends Thread {

        int time;

        Timer(int time){
            this.time = time;
        }

        @Override
        public void run(){
            try {
                Thread.sleep(time);
                if(GameServer.this.getGameState() != GameState.INGAME){
                    GameServer.this.timeOut();
                }
            } catch (InterruptedException e) {
                e.printStackTrace( );
            }
        }
    }
}