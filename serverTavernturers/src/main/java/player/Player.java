package player;

import java.io.BufferedReader;
import java.io.PrintWriter;

public class Player {
    private int id;
    private String name;
    private BufferedReader reader;
    private PrintWriter writer;
    private Boolean exclude;

    public Player(int id, String name, BufferedReader reader, PrintWriter writer){
        this.id = id;
        this.name = name;
        this.reader = reader;
        this.writer = writer;
        this.exclude = false;
    }

    public int getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public BufferedReader getReader() {
        return reader;
    }

    public PrintWriter getWriter(){
        return writer;
    }

    public boolean isExclude(){
        return exclude;
    }

    public void exclude(){
        exclude = true;
    }
}
