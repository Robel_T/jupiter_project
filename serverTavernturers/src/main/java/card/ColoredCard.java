package card;

public class ColoredCard extends Card {
    public ColoredCard(Color color, int number) {
        super(color, number);
    }

    public String getId() {
        char c;
        switch(this.color){
            case RED: c = 'R'; break;
            case BLUE: c = 'B'; break;
            default: c = 'Y'; break;
        }
        return "" + c + number;
    }
}
