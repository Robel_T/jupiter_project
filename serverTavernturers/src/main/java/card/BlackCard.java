package card;

public class BlackCard extends Card {
    public BlackCard(int number) {
        super(Color.BLACK, number);
    }

    public String getId() {
        return "N" + this.number;
    }
}
