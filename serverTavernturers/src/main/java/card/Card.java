package card;

import server.ServerManager;

public abstract class Card {
    protected Color color;
    protected int number;

    public abstract String getId();
    protected Card(Color color, int number){
        this.color = color;
        this.number = number;
    }

    public Color getColor() {
        return color;
    }

    public int getNumber(){
        return number;
    }

    public String toString(){return getId();}

    public static Card recreateCardFromId(String id){
        char cardType = id.charAt(0);
        if(!ServerManager.isNumber(id.substring(1))) return null;
        int cardNumber = Integer.parseInt(id.substring(1));
        switch (cardType){
            case 'N': return new BlackCard(cardNumber);
            case 'B': return new ColoredCard(Color.BLUE, cardNumber);
            case 'R': return new ColoredCard(Color.RED, cardNumber);
            case 'Y': return new ColoredCard(Color.YELLOW, cardNumber);
            case 'D': return new DoorCard(cardNumber);
            default: return null;
        }
    }
}
