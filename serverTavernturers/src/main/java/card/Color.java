package card;

public enum Color {
    COLORLESS,
    BLACK,
    RED,
    BLUE,
    YELLOW
}
