package card;

public class DoorCard extends  Card{

    public DoorCard(int number) {
        super(Color.COLORLESS, number);
    }

    public String getId() {
        return "D" + number;
    }
}
