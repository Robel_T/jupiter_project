package rules;

import card.*;

public class Rule {
    private Rule() { }

    public static Card setLeadingCard(Card playedCard,Card actualLeader){
        //No card was played
        if(actualLeader == null){
            return playedCard;
        }
        //Leading card is Doorcard
        if(actualLeader instanceof DoorCard){
            if(playedCard instanceof DoorCard){
                return actualLeader;
            }else{
                return playedCard;
            }
        }
        //Leading card is Colored
        if(actualLeader instanceof ColoredCard){
            if(playedCard instanceof DoorCard){
                return actualLeader;
            }
            if(playedCard instanceof ColoredCard){
                if((actualLeader.getColor() == playedCard.getColor())
                        && (actualLeader.getNumber() < playedCard.getNumber())){
                    return playedCard;
                }
                return actualLeader;
            }
            return playedCard;
        }
        //Leading card is black
        if(actualLeader instanceof BlackCard){
            if(playedCard instanceof DoorCard || playedCard instanceof ColoredCard){
                return actualLeader;
            }
            if(playedCard instanceof BlackCard && actualLeader.getNumber() > playedCard.getNumber()){
                return actualLeader;
            }
            return playedCard;
        }
        //Otherwise (further developpment)
        return actualLeader;
    }
}
