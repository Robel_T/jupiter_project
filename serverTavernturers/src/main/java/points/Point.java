package points;

public class Point {
    private final int COEFFICIENT_WIN = 20;
    private final int COEFFICIENT_LOS_OR_ZERO = 10;

    private int points[];
    private int nbJoueur;

    public Point(int nbJoueur){
        points = new int[nbJoueur];
        this.nbJoueur = nbJoueur;
    }

    public void updatePoint(int bet[],int result[], int round){
        for(int i = 0; i < this.nbJoueur; i ++){
            if(bet[i] == result[i]){
                if(bet[i] == 0){
                    points[i] += COEFFICIENT_LOS_OR_ZERO * round;
                }
                else {
                    points[i] += COEFFICIENT_WIN * bet[i];
                }
            }
            else{
                if(bet[i] == 0){
                    points[i] -= COEFFICIENT_LOS_OR_ZERO * round;
                }
                else {
                    int malus = bet[i] - result[i];
                    points[i] -= COEFFICIENT_LOS_OR_ZERO * Math.abs(malus);
                }
            }
        }
    }

    public int[] actualPoint(){
       return points;
    }
}
