package cardPackage;

import card.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class CardPackage {
    final public static  int NBFINALROUND = 10;
    private static final int NBITERATIONS = 3;
    final private  int NBMAXCARDCOLOR = 13;
    final private int MAXDOORCARD = 8;
    List <Card>cards = new LinkedList();

    public CardPackage(){
        for(int i = 1; i < NBMAXCARDCOLOR + 1; i ++){
            cards.add(new BlackCard(i));
        }
        for(int i = 1; i < NBMAXCARDCOLOR + 1; i ++){
            cards.add(new ColoredCard(Color.BLUE,i));
        }
        for(int i = 1; i < NBMAXCARDCOLOR + 1; i ++){
            cards.add(new ColoredCard(Color.RED,i));
        }
        for(int i = 1; i < NBMAXCARDCOLOR + 1; i ++){
            cards.add(new ColoredCard(Color.YELLOW,i));
        }
        for(int i = 1; i < MAXDOORCARD + 1; i ++){
            cards.add(new DoorCard(i));
        }
        mix();
    }

    private void mix(){
        Random r = new Random();
        for(int i = 0; i  < NBITERATIONS; i  ++){
            for(int j = 0 ; j < this.cards.size(); j ++){
                change(r.nextInt(this.cards.size()),r.nextInt(this.cards.size()));
            }
        }
    }

    private void change(int i, int j)
    {
        Card temp;
        temp = this.cards.get(i);
        this.cards.set(i,this.cards.get(j));
        this.cards.set(j,temp);
    }

    public int getCardsAvailable(){
        return this.cards.size();
    }

    public List<Card> distribution(int n){
        if(n <= this.cards.size()){
            List<Card> cardsPlayed = this.cards.subList(0,n);
            this.cards = this.cards.subList(n,this.cards.size());
            return cardsPlayed;
        }
        return null;
    }


}
