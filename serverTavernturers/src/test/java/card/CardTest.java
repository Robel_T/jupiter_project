package card;


import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CardTest {
    @Test
    void cardShouldHaveTheRightId(){
        String[] rigthId = {"N1", "Y1", "R1", "B1",
                            "N2", "Y2", "R2", "B2",
                            "N3", "Y3", "R3", "B3",
                            "N4", "Y4", "R4", "B4",
                            "N5", "Y5", "R5", "B5",
                            "N6", "Y6", "R6", "B6",
                            "N7", "Y7", "R7", "B7",
                            "D1", "D2", "D3", "D4"};
        List<Card> cards = new LinkedList<Card>();
        for(int i = 1; i < 8; ++i){
            cards.add(new BlackCard(i));
            cards.add(new ColoredCard(Color.YELLOW, i));
            cards.add(new ColoredCard(Color.RED, i));
            cards.add(new ColoredCard(Color.BLUE, i));
        }
        for(int i = 1; i < 5; ++i){
            cards.add(new DoorCard(i));
        }
        for(int i = 0; i < cards.size(); ++i){
            assertEquals(rigthId[i], cards.get(i).getId());
        }
    }

    @Test
    void aCardMustReturnTheRightColor(){
        Card red = new ColoredCard(Color.RED, 1);
        Card blue = new ColoredCard(Color.BLUE, 1);
        Card yellow = new ColoredCard(Color.YELLOW, 1);
        Card black = new BlackCard(1);
        Card door = new DoorCard(1);
        assertEquals(Color.RED, red.getColor());
        assertEquals(Color.BLUE, blue.getColor());
        assertEquals(Color.YELLOW, yellow.getColor());
        assertEquals(Color.BLACK, black.getColor());
        assertEquals(Color.COLORLESS, door.getColor());
    }
}
