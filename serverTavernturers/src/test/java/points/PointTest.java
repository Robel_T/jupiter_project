package points;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PointTest {

    @Test
    void createPoint(){
        Point p = new Point(5);
        int[] expected = {0,0,0,0,0};
        int[] actualPoint = p.actualPoint();
        for(int i = 0; i < 5; ++i){
            assertEquals(expected[i], actualPoint[i]);
        }
    }

    @Test
    void updatePoint(){
        Point p = new Point(3);

        int bet[] = {2,3,4};
        int result[] = {0,3,5};
        int expected[] = {-20,60,-10};

        p.updatePoint(bet,result,8);
        int actualPoint[] = p.actualPoint();

        for(int i = 0; i < 3; ++i){
            assertEquals(expected[i], actualPoint[i]);
        }


        int bet1[] = {0,5,0};
        int result1[] = {0,5,4};
        int expected1[] = {70,160,-100};

        p.updatePoint(bet1,result1,9);

        int[] actualPoint1 = p.actualPoint();
        for(int i = 0; i < 3; ++i){
            assertEquals(expected1[i], actualPoint1[i]);
        }
    }

}