package rules;


import card.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RuleTest {
    @Test
    void theRuleMustSelectTheRightLeader(){
        Card d1 = new DoorCard(1);
        Card d2 = new DoorCard(2);
        Card r1 = new ColoredCard(Color.RED, 1);
        Card r13 = new ColoredCard(Color.RED, 13);
        Card b6 = new ColoredCard(Color.BLUE, 6);
        Card b7 = new ColoredCard(Color.BLUE,7);
        Card n2 = new BlackCard(2);
        Card n10 = new BlackCard(10);
        assertEquals(Rule.setLeadingCard(d1, null), d1);
        assertEquals(Rule.setLeadingCard(d2, d1), d1);
        assertEquals(Rule.setLeadingCard(r1, d2), r1);
        assertEquals(Rule.setLeadingCard(r13, r1), r13);
        assertEquals(Rule.setLeadingCard(b7, r1), r1);
        assertEquals(Rule.setLeadingCard(b6, b7), b7);
        assertEquals(Rule.setLeadingCard(n2, r13), n2);
        assertEquals(Rule.setLeadingCard(n10, d2), n10);
        assertEquals(Rule.setLeadingCard(d1, n10), n10);
    }

}
