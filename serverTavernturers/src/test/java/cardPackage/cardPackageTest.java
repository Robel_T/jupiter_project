package cardPackage;

import card.Card;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class cardPackageTest {

    @Test
    void nbCardTotal(){
        CardPackage cp = new CardPackage();

        assertEquals(60,cp.getCardsAvailable());
    }


    @Test
    void distribution(){
        CardPackage cp = new CardPackage();
        List l = cp.distribution(5);

        assertEquals(55,cp.getCardsAvailable());
        assertEquals(5,l.size());
    }

    @RepeatedTest(50)
    void twoPlayersMustNotHaveTheSameCards(){
        CardPackage cp = new CardPackage();
        List<Card> cl1 = cp.distribution(20);
        List<Card> cl2 = cp.distribution(20);
        for(Card c1 : cl1){
            for(Card c2 : cl2){
               assertNotEquals(c1.getId(), c2.getId());
            }
        }
    }
}