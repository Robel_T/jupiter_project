package server;

import card.*;
import cardPackage.CardPackage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import player.Player;
import points.Point;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(PowerMockRunner.class)
@PrepareForTest(ServerManager.class)
class GameServerTest {
    @Mock
    private BufferedReader br;
    @Mock
    private PrintWriter pw;
    @Mock
    private LinkedList<Card> stack = new LinkedList<>();
    @Mock
    private Point point = new Point(3);
    @Mock
    private Pair<Card, Player> leader;

    @Test
    void theGameServerMustHaveTheRightNumberOfPlayer(){
        MockitoAnnotations.initMocks(this);
        GameServer gs = new GameServer("XXXXX", 4, true);
        assertTrue(gs.size() == 0);
        gs.addPlayer(pw, br, "test");
        assertTrue(gs.size() == 1);
        gs.addPlayer(pw, br, "test");
        gs.addPlayer(pw, br, "test");
        assertTrue(gs.size() == 3);
    }
    @Test
    void theGameServerMustBeAvailableOnRightTime(){
        MockitoAnnotations.initMocks(this);
        GameServer gs = new GameServer("AAAAA", 3, true);
        assertTrue(gs.isAccessible());
        gs.addPlayer(pw, br, "test");
        gs.addPlayer(pw, br, "test");
        assertTrue(gs.isAccessible());
        gs.addPlayer(pw, br, "test");
        assertFalse(gs.isAccessible());
        GameServer gs2 = new GameServer("BBBBB", 3, true);
        assertTrue(gs2.isAccessible());
        gs2.addPlayer(pw, br, "test");
        gs2.addPlayer(pw, br, "test");
        assertTrue(gs2.isAccessible());
        gs2.createTimer(3000);
        gs2.waitForPlayers();
        assertFalse(gs2.isAccessible());
    }

    @Test
    void aGameServerShouldHaveTheRightId(){
        String id = "R0B3L";
        GameServer gs = new GameServer(id, 0, true);
        assertEquals(id, gs.getIdentification());
    }

    @Test
    void aGameServerShouldReturnTheListOfAllPlayersInJSON(){
        MockitoAnnotations.initMocks(this);
        GameServer gs = new GameServer("CCCCC", 6, true);
        assertEquals("{\"players\":[]}", gs.getPlayers());
        gs.addPlayer(pw, br, "Julien");
        gs.addPlayer(pw, br, "Loris");
        gs.addPlayer(pw, br, "Robel");
        assertEquals("{\"players\":[{\"name\":\"Julien\",\"id\":0}," +
                "{\"name\":\"Loris\",\"id\":1},{\"name\":\"Robel\",\"id\":2}]}", gs.getPlayers());
        gs.addPlayer(pw, br, "Yoann");
        gs.addPlayer(pw, br, "Stefan");
        gs.addPlayer(pw, br, "laMerNoire");
        assertEquals("{\"players\":[{\"name\":\"Julien\",\"id\":0}," +
                "{\"name\":\"Loris\",\"id\":1},{\"name\":\"Robel\",\"id\":2}," +
                "{\"name\":\"Yoann\",\"id\":3},{\"name\":\"Stefan\",\"id\":4}," +
                "{\"name\":\"laMerNoire\",\"id\":5}]}", gs.getPlayers());
    }

    @Test
    void aGameServerMustReturnACorrectListOfCardInJSON(){
        GameServer gs = new GameServer("DDDDD", 0, true);
        List<Card> cards= new LinkedList<>();
        cards.add(new ColoredCard(Color.RED, 2));
        cards.add(new BlackCard(7));
        cards.add(new DoorCard(2));
        cards.add(new ColoredCard(Color.BLUE, 13));
        assertEquals("{\"hand\":[{\"card\":\"R2\"}," +
                "{\"card\":\"N7\"},{\"card\":\"D2\"}," +
                "{\"card\":\"B13\"}]}", gs.getPlayerCards(cards));
    }

    @Test
    void aGameServerMustDistributeTheCorrectNumberOfCards(){
        MockitoAnnotations.initMocks(this);
        GameServer gs = new GameServer("EEEEE", 5, true);
        int[] remainingCards = {60, 55, 50, 45, 40, 35, 30, 25, 20, 15, 10, 5};
        gs.addPlayer(pw, br, "Julien");
        gs.addPlayer(pw, br, "Loris");
        gs.addPlayer(pw, br, "Robel");
        gs.addPlayer(pw, br, "Yoann");
        gs.addPlayer(pw, br, "Stefan");
        for(int i = 0; i <= CardPackage.NBFINALROUND; ++i){
            CardPackage deck = new CardPackage();
            gs.distributeCards(deck, i);
            assertEquals(deck.getCardsAvailable(), remainingCards[i]);
        }
    }

    @Test
    void aGameServerMustReturnACorrectPlayInJSON(){
        MockitoAnnotations.initMocks(this);
        GameServer gs = new GameServer("FFFFF", 3, true);
        Player p = new Player(2, "test", br, pw);
        Card c = new BlackCard(8);
        String result = gs.getPlay("Resultat", new Pair(c, p));
        assertEquals("{\"Resultat\":{\"card\":\"N8\",\"player\":2}}", result);
    }

    @Test
    void aGameServerMustShutDownCorrectly() throws Exception {
        MockitoAnnotations.initMocks(this);
        GameServer gs = new GameServer("GGGGG", 4, true);
        gs.addPlayer(pw, br, "test1");
        gs.addPlayer(pw, br, "test2");
        gs.addPlayer(pw, br, "test3");
        gs.addPlayer(pw, br, "test4");
        gs.shutDown();
        verify(pw, times(4)).close();
        verify(br, times(4)).close();
        assertEquals(gs.getGameState(), GameState.ZOMBIE);
    }

    @Test
    void aGameMustTimeOutCorrectly(){
        GameServer gs = new GameServer("HHHHH", 4, true);
        gs.timeOut();
        assertEquals(gs.getGameState(), GameState.INGAME);
    }

    @Test
    void aGameMustExcludePlayerCorrectly() throws IOException {
        MockitoAnnotations.initMocks(this);
        GameServer gs = new GameServer("IIIII", 3, true);
        Player p1 = gs.addPlayer(pw, br, "test1");
        Player p2 = gs.addPlayer(pw, br, "test2");
        gs.addPlayer(pw, br, "test3");
        gs.excludePlayer(p1);
        verify(pw, times(1)).close();
        verify(br, times(1)).close();
        assertEquals("{\"players\":[{\"name\":\"test2\",\"id\":1}," +
                "{\"name\":\"test3\",\"id\":2}]}", gs.getPlayers());
        gs.excludePlayer(p2);
        verify(pw, times(3)).close();
        verify(br, times(3)).close();
        assertEquals(gs.getGameState(), GameState.ZOMBIE);
    }

    @Test
    void aConnexionMustBeClosedCorrectlyForAnParticularPlayer() throws IOException {
        MockitoAnnotations.initMocks(this);
        GameServer gs = new GameServer("JJJJJ", 3, true);
        Player p1 = gs.addPlayer(pw, br, "test1");
        gs.addPlayer(pw, br, "test2");
        gs.addPlayer(pw, br, "test3");
        gs.closePlayerConnexion(p1);
        verify(pw, times(1)).close();
        verify(br, times(1)).close();
    }

    @Test
    void aConnexionMustBeClosedCorrectlyForAllPlayer() throws IOException {
        MockitoAnnotations.initMocks(this);
        GameServer gs = new GameServer("KKKKK", 3, true);
        gs.addPlayer(pw, br, "test1");
        gs.addPlayer(pw, br, "test2");
        gs.addPlayer(pw, br, "test3");
        gs.closeConnexion();
        verify(pw, times(3)).close();
        verify(br, times(3)).close();
    }

    @Test
    void aServerMustWaitForBets() throws IOException {
        MockitoAnnotations.initMocks(this);
        GameServer gs = new GameServer("LLLLL", 3, true);
        gs.addPlayer(pw, br, "test1");
        gs.addPlayer(pw, br, "test2");
        gs.addPlayer(pw, br, "test3");
        when(br.readLine()).thenReturn("BET 0");
        gs.waitForBets();
        verify(br, times(3)).readLine();
    }

    @Test
    void aGameMustReturnTheCorrectPoints(){
        MockitoAnnotations.initMocks(this);
        GameServer gs = new GameServer("MMMMM", 3, true);
        gs.points = point;
        gs.addPlayer(pw, br, "test1");
        gs.addPlayer(pw, br, "test2");
        gs.addPlayer(pw, br, "test3");
        when(point.actualPoint()).thenReturn(new int[]{500, -90, 140});
        assertEquals("{\"playerPoints\":[{\"id\":0,\"points\":500}," +
                "{\"id\":1,\"points\":-90},{\"id\":2,\"points\":140}]}", gs.getPlayersPoints());
    }

    @Test
    void aTurnMustBeResetCorrectly(){
        MockitoAnnotations.initMocks(this);
        GameServer gs = new GameServer("NNNNN", 3, true);
        leader = new Pair<>(new BlackCard(4), new Player(1, "test", br, pw));
        gs.points = point;
        gs.stack = stack;
        gs.actualLeader = leader;
        gs.resetTurn();
        verify(stack, times(1)).clear();
        assertEquals(Color.COLORLESS, gs.stackColor);
        assertNull(gs.actualLeader.getKey());
        assertNull(gs.actualLeader.getValue());
    }

    @Test
    void aRoundMustBeResetCorrectly(){
        GameServer gs = new GameServer("OOOOO", 3, true);
        gs.addPlayer(pw, br, "test1");
        gs.addPlayer(pw, br, "test2");
        gs.addPlayer(pw, br, "test3");
        int[] table = new int[]{1, 2, 3};
        gs.playerBets = table;
        gs.doneBets = table;
        gs.resetRound();
        for(int i : gs.doneBets){
            assertEquals(0, i);
        }
        for(int i : gs.playerBets){
            assertEquals(0, i);
        }
    }

    @Test
    void aServerMustReturnTheRightPlayersBets(){
        GameServer gs = new GameServer("OOOOO", 3, true);
        gs.addPlayer(pw, br, "test1");
        gs.addPlayer(pw, br, "test2");
        gs.addPlayer(pw, br, "test3");
        int[] table = new int[]{5, 2, 4};
        gs.playerBets = table;
        assertEquals("{\"bets\":[{\"bet\":5,\"id\":0},{\"bet\":2,\"id\":1},{\"bet\":4,\"id\":2}]}",
                gs.getPlayersBets());
    }

    @Test
    void aServerMustMakeAPlayerPlay() throws IOException {
        MockitoAnnotations.initMocks(this);
        GameServer gs = new GameServer("PPPPP", 3, true);
        Player p1 = gs.addPlayer(pw, br, "test1");
        Player p2 = gs.addPlayer(pw, br, "test2");
        gs.addPlayer(pw, br, "test3");
        gs.stackColor = Color.BLUE;
        gs.stack = stack;
        leader = new Pair<>(new ColoredCard(Color.BLUE, 13), p1);
        gs.actualLeader = leader;
        when(br.readLine()).thenReturn("PLAY N1");
        gs.playerTurn(p2);
        verify(stack, times(1)).add(any());
        verify(pw, times(10)).flush();
        verify(pw, times(1)).println("TOKEN");
        verify(br, times(1)).readLine();
    }

    @Test
    void aServerShouldManageTurn(){
        MockitoAnnotations.initMocks(this);
        GameServer gs = spy(new GameServer("QQQQQ", 3, true));
        Player p1 = gs.addPlayer(pw, br, "test1");
        gs.addPlayer(pw, br, "test2");
        gs.addPlayer(pw, br, "test3");
        leader = new Pair<>(new ColoredCard(Color.BLUE, 13), p1);
        gs.actualLeader = leader;
        int[] table = new int[]{1,2,3};
        gs.doneBets = table;
        Mockito.doNothing().when(gs).playerTurn(any());
        gs.playTurn(1);
        //verify(leader, times(2)).getValue();
    }

    @Test
    void aServerShouldManageWhenPlayerPlay(){
        MockitoAnnotations.initMocks(this);
        GameServer gs = spy(new GameServer("RRRRR", 3, true));
        Player p1 = gs.addPlayer(pw, br, "test1");
        Player p2 = gs.addPlayer(pw, br, "test2");
        Player p3 = gs.addPlayer(pw, br, "test3");
        leader = new Pair<>(new ColoredCard(Color.BLUE, 13), p1);
        gs.actualLeader = leader;
        int[] table = new int[]{1,2,3};
        gs.doneBets = table;
        Mockito.doNothing().when(gs).playerTurn(any());
        gs.playTurn(1);
        InOrder inOrder = inOrder(gs);
        inOrder.verify(gs).playerTurn(p2);
        inOrder.verify(gs).playerTurn(p3);
        inOrder.verify(gs).playerTurn(p1);
    }

    @Test
    void aServerMustDoRoundCorrectly(){
        MockitoAnnotations.initMocks(this);
        GameServer gs = spy(new GameServer("SSSSS", 3, true));
        gs.addPlayer(pw, br, "test1");
        gs.addPlayer(pw, br, "test2");
        gs.addPlayer(pw, br, "test3");
        int[] table = new int[]{1,2,3};
        gs.doneBets = table;
        table = new int[]{0, 6, 0};
        gs.playerBets = table;
        point = new Point(3);
        gs.points = point;
        doNothing().when(gs).waitForBets();
        doNothing().when(gs).sendToAllPlayer(anyString());
        doNothing().when(gs).distributeCards(any(), anyInt());
        doNothing().when(gs).resetTurn();
        doReturn(1).when(gs).playTurn(anyInt());
        doReturn("Test").when(gs).getPlayersBets();
        doReturn("Test").when(gs).getPlayersPoints();
        gs.playRound(6);
        verify(gs, times(1)).sendToAllPlayer("BETED Test");
        verify(gs, times(6)).sendToAllPlayer("TURNWINNER 1");
        verify(gs, times(1)).sendToAllPlayer("POINTS Test");
        verify(gs, times(1)).distributeCards(any(), anyInt());
        verify(gs, times(1)).waitForBets();
        verify(gs, times(6)).resetTurn();
        verify(gs, times(6)).playTurn(anyInt());
    }

    @Test
    void aServerMustManageAGameCorrectly(){
         MockitoAnnotations.initMocks(this);
         GameServer gs = spy(new GameServer("TTTTT", 3, true));
         doNothing().when(gs).resetRound();
         doNothing().when(gs).playRound(anyInt());
         gs.playGame();
         verify(gs, times(10)).resetRound();
         verify(gs, times(10)).playRound(anyInt());
    }

    @Test
    void aServerShouldNotManageAGameIfItIsInZombie(){
        MockitoAnnotations.initMocks(this);
        GameServer gs = spy(new GameServer("TTTTT", 3, true));
        gs.state = GameState.ZOMBIE;
        gs.playGame();
        verify(gs, times(0)).resetRound();
        verify(gs, times(0)).playRound(anyInt());
    }

    @Test
    void aServerShouldSendMessageToAllPlayers(){
        MockitoAnnotations.initMocks(this);
        GameServer gs = spy(new GameServer("UUUUU", 4, true));
        gs.addPlayer(pw, br, "test1");
        Player p = gs.addPlayer(pw, br, "test2");
        gs.addPlayer(pw, br, "test3");
        gs.addPlayer(pw, br, "test4");
        gs.excludePlayer(p);
        gs.sendToAllPlayer("This is a test");
        verify(pw, times(3)).println("This is a test");
        verify(pw, times(16)).flush();
    }
}