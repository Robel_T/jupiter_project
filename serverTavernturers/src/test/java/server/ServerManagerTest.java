package server;


import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class ServerManagerTest {
    @Test
    void theManagerServerMustNotHaveTwoServerWithTheSameName() {
        ServerManager sm = new ServerManager(0);
        List<String> idList = new LinkedList<>();
        for(int i = 0; i < 2000; ++i) {
            idList.add(sm.generateNewGameServer(1, true));
        }
        for(int i = 0; i < idList.size(); ++i){
            for(int j = i + 1; j < idList.size(); ++j){
                assertNotEquals(idList.get(i), idList.get(j));
            }
        }
    }

    @Test
    void theManagerCanKnowIfAStringCanBeCastToNumber(){
        ServerManager sm = new ServerManager(0);
        assertTrue(sm.isNumber("42"));
        assertFalse(sm.isNumber("this is a test"));
        assertFalse(sm.isNumber("4.89"));
    }
}
